-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 06, 2017 at 08:42 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `oswt`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_status` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `category_created` datetime NOT NULL,
  `category_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_id`, `category_name`, `category_status`, `category_created`, `category_updated`) VALUES
(1, 'Name of the category sdd', 'a', '2016-11-29 10:20:06', '2017-02-27 05:00:22'),
(2, 'updated Category', 'a', '2016-11-29 10:20:35', '2017-03-24 11:04:12'),
(3, 'New Category', 'a', '2016-12-10 05:26:09', '2016-12-13 02:06:15'),
(4, 'asdsadasdasdasd', 'a', '2017-02-27 05:00:32', '2017-02-27 05:00:32'),
(5, '	updated Category', 'a', '2017-04-10 11:24:09', '2017-04-10 11:24:09');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `description` varchar(100) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User'),
(3, 'operator', ''),
(4, 'managers', 'manager - who manages blog post and operators ');

-- --------------------------------------------------------

--
-- Table structure for table `keys`
--

CREATE TABLE `keys` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `key` varchar(40) CHARACTER SET utf8 NOT NULL,
  `level` int(2) NOT NULL,
  `ignore_limits` tinyint(1) NOT NULL DEFAULT '0',
  `is_private_key` tinyint(1) NOT NULL DEFAULT '0',
  `ip_addresses` text CHARACTER SET utf8,
  `date_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `keys`
--

INSERT INTO `keys` (`id`, `user_id`, `key`, `level`, `ignore_limits`, `is_private_key`, `ip_addresses`, `date_created`) VALUES
(1, 0, '12345', 0, 0, 0, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(15) CHARACTER SET utf8 NOT NULL,
  `login` varchar(100) CHARACTER SET utf8 NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` int(11) NOT NULL,
  `uri` varchar(255) CHARACTER SET utf8 NOT NULL,
  `method` varchar(6) CHARACTER SET utf8 NOT NULL,
  `params` text CHARACTER SET utf8,
  `api_key` varchar(40) CHARACTER SET utf8 NOT NULL,
  `ip_address` varchar(45) CHARACTER SET utf8 NOT NULL,
  `time` int(11) NOT NULL,
  `rtime` float DEFAULT NULL,
  `authorized` varchar(1) CHARACTER SET utf8 NOT NULL,
  `response_code` smallint(3) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ns_categories`
--

CREATE TABLE `ns_categories` (
  `ns_category_id` int(11) NOT NULL,
  `ns_category_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `ns_category_slug` varchar(255) CHARACTER SET utf8 NOT NULL,
  `ns_category_left` int(11) NOT NULL DEFAULT '0',
  `ns_category_right` int(11) NOT NULL DEFAULT '0',
  `ns_category_parent` int(11) NOT NULL DEFAULT '0',
  `ns_category_image` varchar(255) CHARACTER SET utf8 NOT NULL,
  `ns_category_icon` varchar(255) CHARACTER SET utf8 NOT NULL,
  `ns_category_status` varchar(1) CHARACTER SET utf8 NOT NULL COMMENT 'a - activated | d - deactivated | t - Trashed',
  `ns_category_created` datetime NOT NULL,
  `ns_category_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ns_categories`
--

INSERT INTO `ns_categories` (`ns_category_id`, `ns_category_name`, `ns_category_slug`, `ns_category_left`, `ns_category_right`, `ns_category_parent`, `ns_category_image`, `ns_category_icon`, `ns_category_status`, `ns_category_created`, `ns_category_updated`) VALUES
(1, 'Parent', 'parent', 3, 4, 0, '', 'asd', 'a', '2017-03-21 11:44:23', '2017-03-21 11:47:17'),
(2, 'parent1', 'parent1', 1, 2, 0, '', '', 'a', '2017-03-21 11:46:31', '2017-03-21 11:46:31');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `post_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `post_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `post_slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `post_description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `post_meta_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `post_meta_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `post_meta_keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `post_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `post_image_alt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `post_video` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `post_email` text COLLATE utf8_unicode_ci NOT NULL,
  `post_status` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `post_created` datetime NOT NULL,
  `post_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`post_id`, `category_id`, `user_id`, `post_title`, `post_slug`, `post_description`, `post_meta_title`, `post_meta_description`, `post_meta_keywords`, `post_image`, `post_image_alt`, `post_video`, `post_email`, `post_status`, `post_created`, `post_updated`) VALUES
(1, 1, 4, 'New post ', 'name-of-the-category', '<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.</p>', '', '', '', '154af38e39a19606c42b0b1f29056a14.png', '', 'https://www.youtube.com/watch?v=J7_1MU3gDk0', '', 'a', '2016-12-10 07:42:14', '2017-05-03 12:33:09'),
(2, 2, 4, 'demo demo123', 'demo-demo123', '<h6 id="mcetoc_1b4ghms8f0" style="text-align: center;"><strong>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.</strong></h6>\r\n<h2 id="mcetoc_1b3s1gi3t0" style="text-align: center;"><strong><span style="color: #ff0000;">Hyderabad </span></strong></h2>\r\n<table style="width: 644px;">\r\n<tbody>\r\n<tr>\r\n<td style="width: 146.133px;">ICICI</td>\r\n<td style="width: 150.867px;">&nbsp;sdfsdf</td>\r\n<td style="width: 49px;">&nbsp;sdfsdf</td>\r\n<td style="width: 48px;">&nbsp;</td>\r\n<td style="width: 49px;">&nbsp;</td>\r\n<td style="width: 48px;">&nbsp;</td>\r\n<td style="width: 49px;">&nbsp;</td>\r\n<td style="width: 48px;">&nbsp;</td>\r\n<td style="width: 49px;">&nbsp;</td>\r\n</tr>\r\n<tr>\r\n<td style="width: 146.133px;">HDFC</td>\r\n<td style="width: 150.867px;">&nbsp;sdfsdf</td>\r\n<td style="width: 49px;">&nbsp;sdfsdf</td>\r\n<td style="width: 48px;">&nbsp;</td>\r\n<td style="width: 49px;">&nbsp;</td>\r\n<td style="width: 48px;">&nbsp;</td>\r\n<td style="width: 49px;">&nbsp;</td>\r\n<td style="width: 48px;">&nbsp;</td>\r\n<td style="width: 49px;">&nbsp;</td>\r\n</tr>\r\n<tr>\r\n<td style="width: 146.133px;">YES BANK</td>\r\n<td style="width: 150.867px;">&nbsp;sdfsdf</td>\r\n<td style="width: 49px;">&nbsp;sdfsdfsdf</td>\r\n<td style="width: 48px;">&nbsp;</td>\r\n<td style="width: 49px;">&nbsp;</td>\r\n<td style="width: 48px;">&nbsp;</td>\r\n<td style="width: 49px;">&nbsp;</td>\r\n<td style="width: 48px;">&nbsp;</td>\r\n<td style="width: 49px;">&nbsp;</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>&nbsp;</p>', 'sdfdsf', 'sdfsdf', 'sdfsdf', '87c55c14452590e06e502b6ff8513fb0.png', '', 'https://www.youtube.com/watch?v=J7_1MU3gDk0', '', 'a', '2016-12-10 07:49:59', '2017-05-03 12:33:44'),
(3, 1, 4, 'with category', 'with-category', '<p>sad asdsadad</p>', 'with-category', 'with-category', 'with-category', '134323233123630adbba7f0685d48822.jpg', 'asdsadasdasdasdasda', 'https://www.youtube.com/watch?v=J7_1MU2gDk0', '', 't', '2016-12-14 03:30:35', '2017-05-03 12:42:35'),
(4, 1, 4, 'colleges', 'colleges', '<p>collegescollegescollegescollegescolleges</p>', 'colleges', 'colleges', 'colleges', '', 'colleges', '', '', 'd', '2017-03-24 11:31:16', '2017-05-03 12:42:32'),
(6, 1, 4, 'banksssss', 'banksssss', '<p>banksssssbanksssssbanksssssbanksssss</p>', 'banksssss', 'banksssss', 'banksssss', '', 'banksssssbanksssssbanksssss', '', '', 'a', '2017-03-24 11:40:52', '2017-05-03 12:32:42'),
(7, 1, 4, 'gfdgdg', 'gfdgdg', '<p>gfdgdggfdgdggfdgdggfdgdggfdgdggfdgdg</p>', 'gfdgdg', 'gfdgdg', 'gfdgdg', '', 'gfdgdggfdgdggfdgdggfdgdg', '', '', 'a', '2017-03-24 11:41:12', '2017-05-03 12:32:56'),
(8, 2, 4, 'fsdgfdhh', 'fsdgfdhh', '<p>fsdgfdhhfsdgfdhhfsdgfdhhfsdgfdhh</p>', 'fsdgfdhh', 'fsdgfdhh', 'fsdgfdhh', '', 'fsdgfdhhfsdgfdhhfsdgfdhh', '', '', 't', '2017-03-24 11:41:42', '2017-03-24 11:41:56'),
(9, 1, 4, 'e', 'rer', '<p>t is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>', 'srwer', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', '', 'ddsd', '', '', 't', '2017-05-01 15:51:51', '2017-05-03 12:29:47'),
(10, 2, 4, 'dsd', 'fsd', '<p>t is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>', 'dsd', 'dsdd', 'sd', '', 'sds', '', '', 'a', '2017-05-01 15:52:26', '2017-05-03 12:33:25'),
(11, 1, 3, 'xcs', 'dsd', '<p>t is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>', 'sdsd', 'dsdsd', 'sds', '', 'wewe', '', '', 't', '2017-05-01 15:52:52', '2017-05-03 12:32:36'),
(12, 1, 3, 'fdsf', 'dfsdf', '<p>fdfsdfsd</p>', 'fdf', 'dfdf', 'sdfsdf', '', 'fdds', '', '', 'a', '2017-05-06 12:02:14', '2017-05-06 12:03:03');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) CHARACTER SET utf8 NOT NULL,
  `username` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 NOT NULL,
  `salt` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `email` varchar(100) CHARACTER SET utf8 NOT NULL,
  `activation_code` varchar(40) CHARACTER SET utf8 DEFAULT NULL,
  `forgotten_password_code` varchar(40) CHARACTER SET utf8 DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) CHARACTER SET utf8 DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `last_name` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `company` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `phone` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `photo` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `date_of_birth` datetime DEFAULT NULL,
  `gender` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `device_type` varchar(255) CHARACTER SET utf8 NOT NULL,
  `device_token` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`, `photo`, `date_of_birth`, `gender`, `device_type`, `device_token`) VALUES
(1, '127.0.0.1', 'administrator', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', '', 'admin@admin.com', '3e867f716dffe21418514c75a27e922e1defe2cd', NULL, NULL, 'ONxEYB/516TUQSAhCP4ruu', 1268889823, 1494052344, 1, 'Admin', 'istrator', 'ADMIN', '1234567890', NULL, '0000-00-00 00:00:00', '', '', ''),
(2, '::1', NULL, '$2y$08$D/2OpaWzcYTiy/rF7cxj9u/oVH4nelopSAHv.ZrrzHNAsQ36GWCgm', NULL, 'shyambabu2109@gmail.com', NULL, 'deRZnhPV1W-3PA3jTsSTs.460dd6f7d9a1498132', 1470897385, NULL, 1470814539, 1473365295, 1, 'shyam', 'babu', 'OS Web Technologies', '8688873888', NULL, '0000-00-00 00:00:00', '', '123', '123'),
(3, '::1', NULL, '$2y$08$xTrLkhLU9Z2/rOsrvsgNbegiWOc.PsdheuHVAUqDEjDxfHutUPfdy', NULL, 'alekhya24anusha@gmail.com', NULL, NULL, NULL, NULL, 1488193553, 1494052259, 1, 'operator', 'op', 'wsn', '9999999999', NULL, NULL, NULL, '', ''),
(4, '::1', NULL, '$2y$08$NGyx/XvygYGY81IuileuyuhVb1hgbPD/hAJ4Niv6.r9ddTsYd9Y46', NULL, 'anusha@oswebtechnologies.com', NULL, NULL, NULL, NULL, 1493633631, 1494052377, 1, 'anusha', 'paladi', 'oswt', '8790176144', NULL, NULL, NULL, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(44, 1, 1),
(45, 1, 2),
(47, 2, 1),
(50, 3, 3),
(52, 4, 4);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `keys`
--
ALTER TABLE `keys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ns_categories`
--
ALTER TABLE `ns_categories`
  ADD PRIMARY KEY (`ns_category_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`post_id`),
  ADD UNIQUE KEY `post_slug` (`post_slug`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `keys`
--
ALTER TABLE `keys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ns_categories`
--
ALTER TABLE `ns_categories`
  MODIFY `ns_category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `post_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
