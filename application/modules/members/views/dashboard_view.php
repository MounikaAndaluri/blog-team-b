 <style>
        .page-banner {
            height: 0px;
        }
        .bg-transparent {
            background-color: #3F51B5;
            transition: all 0.6s ease;
            -webkit-transition: all 0.6s ease;
            -moz-transition: all 0.6s ease;
            -o-transition: all 0.6s ease;
        }
        .padding-top {
            padding-top: 150px;
        }
        .result-count-wrapper-test{
            text-align: center;
            background-color: #fafafa;
            line-height: 60px;
            border-left: 1px solid #cdcdcd;
            border-right: 1px solid #cdcdcd;
            border-bottom: 1px solid #cdcdcd;
            cursor: pointer;
        }
        .cust-form-control{
            border: 1px solid #ffdd00;
        }
    </style>
 
  <div id="wrapper-content"><!-- MAIN CONTENT-->
            <div class="main-content">
                <section class="page-banner tour-result">
                    <div class="container">
                        <div class="page-title-wrapper">
                            <div class="page-title-content">
                                <ol class="breadcrumb">
                                    <li><a href="index.html" class="link home">Home</a></li>
                                    <li class="active"><a href="#" class="link">Tour</a></li>
                                </ol>
                                <div class="clearfix"></div>
                                <h2 class="captions">Tour</h2></div>
                        </div>
                    </div>
                </section>
                <div class="page-main">
                    
                    <div class="clearfix"></div>
                    <div class="tour-result-main padding-top padding-bottom">
                        <div class="container">
                            
                            <!-- <div class="list-continents">
                                <div class="list-continent-wrapper"><a href="#" class="continent"><i class="icons fa fa-map-marker"></i><span class="text">europe</span></a></div>
                                <div class="list-continent-wrapper"><a href="#" class="continent"><i class="icons fa fa-map-marker"></i><span class="text">america</span></a></div>
                                <div class="list-continent-wrapper"><a href="#" class="continent"><i class="icons fa fa-map-marker"></i><span class="text">asian</span></a></div>
                                <div class="list-continent-wrapper"><a href="#" class="continent"><i class="icons fa fa-map-marker"></i><span class="text">africa</span></a></div>
                                <div class="list-continent-wrapper"><a href="#" class="continent"><i class="icons fa fa-map-marker"></i><span class="text">middle east</span></a></div>
                            </div> -->
                            <div class="result-meta">
                                <div class="row">
                                    <div class="col-lg-4 col-md-12">
                                    <div class="result-count-wrapper-test"><img src="<?php echo base_url(); ?>assets/gyms/assets/images/avatar/avatar-05.png" style="padding:10px;border-radius:50px;"></div>

                                    <div class="result-count-wrapper-test">Settings</div>
                                    <div class="result-count-wrapper-test">My GYM's</div>
                                    <div class="result-count-wrapper-test">Favourites</div>
                                    <div class="result-count-wrapper-test">Payment History</div>
                                       
                                    </div>
                                    <div class="col-lg-8 col-md-12">
                                        <div class="result-filter-wrapper">
                                            <form><label class="result-filter-label">Sort by :</label>

                                                <div class="selection-bar">
                                                    <div class="select-wrapper"><select name="Recommended" class="custom-select selectbox">
                                                        <option value="" disabled="disabled" selected="selected" hidden="hidden">Price</option>
                                                        <option value="1">Low - High</option>
                                                        <option value="2">High - Low</option>
                                                    </select></div>
                                                    <div class="select-wrapper"><select name="Shortest" class="custom-select selectbox">
                                                        <option value="" disabled="disabled" selected="selected" hidden="hidden">Rating</option>
                                                        <option value="5">5</option>
                                                        <option value="4">4</option>
                                                        <option value="3">3</option>
                                                        <option value="2">2</option>
                                                        <option value="1">1</option>
                                                    </select></div>
                                                    <div class="select-wrapper"><select name="Nearby airport" class="custom-select selectbox"><!--option(value="" disabled selected hidden) price-->
                                                        <option value="expensive">A-Z</option>
                                                        <option value="cheap">Z-A</option>
                                                    </select></div>
                                                    <div class="select-wrapper"><select name="Time" class="custom-select selectbox">
                                                        <option value="" disabled="disabled" selected="selected" hidden="hidden">View</option>
                                                        <option value="5">List view</option>
                                                        <option value="4">Map view</option>
                                                    </select></div>
                                                </div>
                                            </form>
                                        </div>
                                        <br>
                                        
                                        <div class="test-calss" id="test">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label for="firstname">First Name</label>
                                                    <input type="text" class="form-control cust-form-control" placeholder="First Name" id="firstname">
                                                </div>
                                                <div class="col-md-6">
                                                    <label for="lastname">Last Name</label>
                                                    <input type="text" class="form-control cust-form-control" placeholder="Last Name" id="lastname">
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label for="email">Email</label>
                                                    <input type="text" class="form-control cust-form-control" placeholder="Email" id="email">
                                                </div>
                                                <div class="col-md-6">
                                                    <label for="phoneno">Phone no</label>
                                                    <input type="text" class="form-control cust-form-control" placeholder="Last Name" id="phoneno">
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label for="dob">Date of birth</label>
                                                    <div class="input-group date" data-provide="datepicker">
                                                        <input type="text" class="form-control cust-form-control" id="dob">
                                                        <div class="input-group-addon">
                                                            <span class="glyphicon glyphicon-th"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Gender</label><br>
                                                    <label class="radio-inline"><input type="radio" name="optradio">Option 1</label>
                                                    <label class="radio-inline"><input type="radio" name="optradio">Option 2</label>

                                                </div>
                                            </div>
                                            <hr>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label for="phoneno">Change Password</label>
                                                    <input type="password" class="form-control cust-form-control" placeholder="************" id="changepwd">
                                                </div>
                                                <div class="col-md-6">
                                                <br>
                                                <button class="btn btn-default">Change password</button>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        
                                    </div>
                                </div>
                            </div>
                           
                            
                        </div>
                    </div>
                </div>
            </div>
           </div>