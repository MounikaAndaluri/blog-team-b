<div id="wrapper-content">
					<!-- MAIN CONTENT-->
					<div class="main-content">
												<div class="page-register rlp">
	<div class="container">
		<div class="register-wrapper rlp-wrapper">
			<div class="register-table rlp-table">
				
				<a href="<?php echo base_url(); ?>">
				<img src="<?php echo base_url()."assets/gym/assets/images/logo/logo-2.png"; ?>" class="login" alt="logo"/>
				</a>				<div class="register-title rlp-title">
					create your account and join with us!
				</div>
								<div id="infoMessage"><?php echo $message;?></div>
								
				<?php echo form_open("members/register");?>
					<div class="register-form bg-w-form rlp-form">
						<div class="row">
							<div class="col-md-6">
								<label for="regname" class="control-label form-label">
									First Name<?php //echo lang('create_user_fname_label', 'first_name');?> 
									<span class="required">*</span>
								</label>
								 <?php echo form_input($first_name); ?>
								<label for="regname" class="error username"></label>
							</div>
							
							<div class="col-md-6">
								<label for="regname" class="control-label form-label">
									Last Name <?php //echo lang('create_user_lname_label', 'last_name');?>
									<span class="required">*</span>
								</label>
								 <?php echo form_input($last_name);?>
								<label for="regname" class="error username"></label>
							</div>


							<div class="col-md-6">
								<label for="regemail" class="control-label form-label">
									Email 
									<span class="required">*</span>
								</label>
								 <?php
								      if($identity_column!=='email') {
								          echo '<p>';
								          echo lang('create_user_identity_label', 'identity');
								          echo '<br />';
								          echo form_error('identity');
								          echo form_input($identity);
								          echo '</p>';
								      }
								      ?> 
								      <?php echo form_input($email);
								 ?>
								<label for="regemail" class="error email"></label>
							</div>

						<div class="col-md-6">
								<label for="regname" class="control-label form-label">
									Mobile <?php //echo lang('create_user_lname_label', 'last_name');?>  
									<span class="required">*</span>
								</label>
								<?php echo form_input($phone);?>
								<label for="regname" class="error username"></label>
							</div>

							<div class="col-md-6">
								<label for="password" class="control-label form-label">
									Password 
									<span class="required">*</span>
								</label>
								<?php echo form_input($password);?>
								<label for="password" class="error password"></label>
							</div>

							<div class="col-md-6">
								<label for="reregpassword" class="control-label form-label">
									Confirm Password 
									<span class="required">*</span>
								</label>
								<?php echo form_input($password_confirm);?>
								<label for="reregpassword" class="error repassword"></label>
							</div>

							<div class="col-md-6">
								<div class="register-captcha">
									<div class="g-recaptcha" data-sitekey="6LfiqiITAAAAAJXMkrflPwIoDFe2_RgGcKOHxcrD"></div>
									<input id="regrecaptcha" type="hidden" name="recaptcha" value="" data-validation-error-msg-required="Please authentication I&#039;m not a robot."/>
								</div>
							</div>
							
							<div class="col-md-6">
								<div class="register-terms">
									<div class="checkbox-terms">
										<input type="checkbox" value="yes" name="agree" id="agree" class="" aria-invalid="false" data-validation-error-msg-required="To register for membership, you must agree to the terms and conditions of our.">
										<div class="content">
											I agree to the <a href="../term-of-service/index.html" target="_blank">terms &amp; conditions</a>.										</div>
										<label for="agree" class="error"></label>
									</div>
								</div>
							</div>
							
						</div>
					</div>

					<div class="register-submit">
						<a href="<?php echo base_url(); ?>" class="btn btn-cancel">Cancel</a>	
						<input type="submit" class="btn btn-register btn-maincolor" name="submit" value="Create Account"/>
					</div>
				<?php echo form_close();?>
			</div>
		</div>
	</div>
</div>
					</div>
					<!-- MAIN CONTENT-->
				</div>
				<!-- PAGE WRAPPER -->






<div class="clearfix"></div>






<?php /*echo lang('create_user_heading');?></h1>
<p><?php echo lang('create_user_subheading');?></p>

<div id="infoMessage"><?php echo $message;?></div>

<?php echo form_open("members/register");?>

      <p>
            <?php echo lang('create_user_fname_label', 'first_name');?> <br />
            <?php echo form_input($first_name);?>
      </p>

      <p>
            <?php echo lang('create_user_lname_label', 'last_name');?> <br />
            <?php echo form_input($last_name);?>
      </p>
      
      <?php
      if($identity_column!=='email') {
          echo '<p>';
          echo lang('create_user_identity_label', 'identity');
          echo '<br />';
          echo form_error('identity');
          echo form_input($identity);
          echo '</p>';
      }
      ?>

      <p>
            <?php echo lang('create_user_company_label', 'company');?> <br />
            <?php echo form_input($company);?>
      </p>

      <p>
            <?php echo lang('create_user_email_label', 'email');?> <br />
            <?php echo form_input($email);?>
      </p>

      <p>
            <?php echo lang('create_user_phone_label', 'phone');?> <br />
            <?php echo form_input($phone);?>
      </p>

      <p>
            <?php echo lang('create_user_password_label', 'password');?> <br />
            <?php echo form_input($password);?>
      </p>

      <p>
            <?php echo lang('create_user_password_confirm_label', 'password_confirm');?> <br />
            <?php echo form_input($password_confirm);?>
      </p>


      <p><?php echo form_submit('submit', lang('create_user_submit_btn'));?></p>

<?php echo form_close(); */ ?>
