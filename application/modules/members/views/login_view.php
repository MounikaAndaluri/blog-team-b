



<div class="">
<div id="wrapper-content">
					<!-- MAIN CONTENT-->
					<div class="main-content">
												<div class="page-login rlp">
	<div class="container">
		<div class="login-wrapper rlp-wrapper">
			<div class="login-table rlp-table">
				
				<a href="<?php echo base_url(); ?>">
					<img src="#" class="login" alt="logo"/></a>				
				<div class="login-title rlp-title">
					sign in to your <?php echo $this->config->item('site_name'); ?> account!				
				</div>
				<div id="infoMessage"><?php echo $message; ?></div>
				<?php echo form_open("members/login");?>
					<div class="login-form bg-w-form rlp-form">
						<div class="row">
							<div class="col-md-12">
								<label for="regemail" class="control-label form-label">
									email 
									<span class="required">*</span>
								</label>
								<input id="identity" type="email" placeholder="" name="identity" class="form-control form-input" value="">
								
							</div>
							<div class="col-md-12">
								<label for="regpassword" class="control-label form-label">
									password 
									<span class="required">*</span>
								</label>
								<input id="password" type="password" name="password" placeholder="" class="form-control form-input">
							</div>
						</div>
						<div class="">
							<input id="remember" name="remember" type="checkbox" class="check"/>
							<label for="remember" class="type-checkbox">
								Remember me							</label>
						</div>
					</div>
					<div class="login-submit">
						
												
						<input type="submit" class="btn btn-maincolor" name="submit" value="sign in" />
						<a href="<?php echo base_url(); ?>" class="btn btn-cancel">Cancel</a>					
					</div>
				<?php echo form_close();?>
				<p class="title-sign-in">
				Don't have a <?php echo $this->config->item('site_name'); ?> account yet?<a href="<?php echo base_url("members/register"); ?>" class="link signin">create now!</a>				</p>
			</div>
		</div>
	</div>
</div>
					</div>
					<!-- MAIN CONTENT-->
				</div>

</div>

