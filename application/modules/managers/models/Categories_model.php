<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class categories_model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}

	public function getCategories($inputs = null){
		$categories = null;

		$page = isset($inputs['page']) ? $inputs['page'] : 0;
			$per_page = isset($inputs['per_page']) ? $inputs['per_page'] : 10;

		$query = " SELECT * FROM categories ".
				 " WHERE category_status = 'a' ".
				 " ORDER BY category_updated DESC ".
				 " LIMIT $page, $per_page ";


		$categories = $this->db->query($query)->result_array();
		
		return $categories;
	}
	public function get_trash_categories($inputs=null){
		$category = null;
		$query = null;
		$page = isset($inputs['page']) ? $inputs['page'] : 0;
		$per_page = isset($inputs['per_page']) ? $inputs['per_page'] : 10;
		$query = "SELECT * FROM categories ".
		" WHERE categories.category_status = 't' ".
		" LIMIT $page, $per_page ";

		$categories = $this->db->query($query)->result_array();
		return $categories;	
	}

	public function getCategoriesSugg($cat) {
		$categories = null;
		$query = null;
		$string =  $cat['term'];

		$query = " SELECT categories.category_id, categories.category_name ".
							" FROM categories ".
							" WHERE categories.category_name LIKE '%$string%' ".
							" AND categories.category_status = 'a' ".
							" LIMIT 0, 10 ";
							
		$categories = $this->db->query($query)->result_array();
		return $categories;
	}

	public function getCategory($cid=null){
		if($cid != null) {
			$category = null;
			$query = null;

				$query = " SELECT * FROM categories ".
						" WHERE category_id = '$cid' ".
						" AND categories.category_status = 'a' ";

				return $this->db->query($query)->row_array();
		}else{
			return false;
		}
	}





}
