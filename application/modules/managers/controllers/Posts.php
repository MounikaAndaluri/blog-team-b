<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Posts extends MY_Controller {
	public $data;
	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url','language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('admin');
		$this->load->model('managers/posts_model');

		if ($this->ion_auth->logged_in())
		{
			if (!$this->ion_auth->in_group('managers'))
			{
				redirect(base_url().'members/login', 'refresh');
			}
		}else{
			redirect(base_url().'members/login', 'refresh');
		}
	}

	public function index() {
        // set the flash data error message if there is one
        $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

        $config = array();
           $config["base_url"] = base_url()."managers/posts";
           $total_row = $this->db->select('post_id')->from('posts')->where('post_status !=','t')->count_all_results();
           $config["total_rows"] = $total_row;
           $config["per_page"] = 10;
           $config['num_links'] = 8;
           //$config["uri_segment"] = 4;
           $config['page_query_string'] = TRUE;

           /* This Application Must Be Used With BootStrap 3 *  */
           $config['full_tag_open'] = '<ul class="pagination pagination-sm no-margin pull-right">';
           $config['full_tag_close'] ="</ul>";

           $config['num_tag_open'] = "<li>";
           $config['num_tag_close'] = '</li>';

           $config['cur_tag_open'] = '<li class="active"><a href="javascript:void();">';
           $config['cur_tag_close'] = "</a></li>";

           $config['next_tag_open'] = "<li>";
           $config['next_tagl_close'] = "</ul>";

           $config['prev_tag_open'] = "<li>";
           $config['prev_tagl_close'] = "</ul>";

           $config['first_tag_open'] = "<li>";
           $config['first_tagl_close'] = "</li>";

           $config['last_tag_open'] = "<li>";
           $config['last_tagl_close'] = "</li>";

           $this->pagination->initialize($config);
           //$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $page = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
           $inputs['page'] = $page;
           $inputs['per_page'] = $config['per_page'];

        $posts =  $this->posts_model->getPosts($inputs);

        $this->data['posts'] = $posts;
        /*print_r($bloodbanks);
        exit();*/
		
        $this->data['title'] = "Posts";
        $this->data['view'] = 'managers/posts/home_view';
        $this->data['posts'] = $posts;
        $this->template->managerTemplate($this->data);
    }

    public function add_post(){
    	// set the flash data error message if there is one
    	$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
    	

    	$config = array(
    			
    			array(
    					'field' => 'postname',
    					'label' => 'Post name',
    					'rules' => 'required',
    			),
    			array(
    					'field' => 'postslug',
    					'label' => 'Post slug',
    					'rules' => 'required',
    			),
    			array(
    					'field' => 'postmetatitle',
    					'label' => 'Post meta title',
    					'rules' => 'required',
    			),
    			array(
    					'field' => 'postmetadesc',
    					'label' => 'Post Meta Description',
    					'rules' => 'required',
    			),
    			array(
    					'field' => 'postmetakeyword',
    					'label' => 'Post post meta keyword',
    					'rules' => 'required',
    			),
    			array(
    					'field' => 'postdesc',
    					'label' => 'Post Description',
    					'rules' => 'required',
    			),
    			array(
    					'field' => 'postvideo',
    					'label' => 'Post Video',
    					'rules' => 'valid_url',
    					
    			),
    			array(
    					'field' => 'postimagealt',
    					'label' => 'Post Image alt',
    					'rules' => 'required',
    					
    			),
    			array(
    					'field' => 'categoryid',
    					'label' => 'Category',
    					'rules' => 'required',
    			),
    			
    	);
    	$this->form_validation->set_rules($config);
    	
    	if ($this->form_validation->run() === TRUE)
    	{
    		
    		$pimage = "";
    		$img_config['upload_path'] = APPPATH.'../images/posts/';
    		$img_config['allowed_types'] = 'jpg|png|gif';
    		$img_config['encrypt_name'] = TRUE;
    		
    		$this->load->library('upload', $img_config);
    		
    		if (!$this->upload->do_upload('postimage')) {
    			$error = array('error' => $this->upload->display_errors());
    			//print_r($error);
    			//exit();
    		} else{
    			$postimage = array('upload_data' => $this->upload->data());
    			$pimage = $postimage['upload_data']['file_name'];
    		}
    		
    		$postslug = $this->create_unique_slug($this->input->post('postslug'), 'posts', 'post_slug');
    		$user_id = $this->ion_auth->user()->row()->id;

    		$cdata = array(
    				'category_id' => $this->input->post('categoryid'),
                    'user_id' => $user_id,
    				'post_title' => $this->input->post('postname'),
    				'post_slug' => $postslug,
    				'post_meta_title' => $this->input->post('postmetatitle'),
    				'post_meta_description' => $this->input->post('postmetadesc'),
    				'post_meta_keywords' => $this->input->post('postmetakeyword'),
    				'post_description' => $this->input->post('postdesc'),
    				'post_image' => $pimage,
    				'post_image_alt' => $this->input->post('postimagealt'),
    				'post_video' => $this->input->post('postvideo'),
    				'post_status' => 'a',
    				'post_created' => date('Y-m-d H:i:s'),
    				'post_updated' => date('Y-m-d H:i:s'),
    		);
    		
    		$this->db->insert('posts', $cdata);
    		
    		$this->data['message'] = "post add successfully";
    	}
    	
    	$this->data['title'] = "Add Post";
    	$this->data['view'] = 'managers/posts/add_post_view';
    	$this->template->managerTemplate($this->data);
    }
    
    
    public function edit_post($pid=null){
    	
    	// set the flash data error message if there is one
    	$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
    	
    	
    	$config = array(
    			
    			array(
    					'field' => 'postname',
    					'label' => 'Post name',
    					'rules' => 'required',
    			),
    			array(
    					'field' => 'postslug',
    					'label' => 'Post slug',
    					'rules' => 'required',
    			),
    			array(
    					'field' => 'postmetatitle',
    					'label' => 'Post meta title',
    					'rules' => 'required',
    			),
    			array(
    					'field' => 'postmetadesc',
    					'label' => 'Post Meta Description',
    					'rules' => 'required',
    			),
    			array(
    					'field' => 'postmetakeyword',
    					'label' => 'Post post meta keyword',
    					'rules' => 'required',
    			),
    			array(
    					'field' => 'postdesc',
    					'label' => 'Post Description',
    					'rules' => 'required',
    			),
    			array(
    					'field' => 'postvideo',
    					'label' => 'Post Video',
    					'rules' => 'valid_url',
    					
    			),
    			array(
    					'field' => 'categoryid',
    					'label' => 'Category',
    					'rules' => 'required',
    			),
    			
    	);
    	
    	
    	$this->form_validation->set_rules($config);
    	
    	
    	if ($this->form_validation->run() === TRUE){
    		
    		$pimage = "";
    		$img_config['upload_path'] = APPPATH.'../images/posts/';
    		$img_config['allowed_types'] = 'jpg|png|gif';
    		$img_config['encrypt_name'] = TRUE;
    		
    		$this->load->library('upload', $img_config);
    		
    		if (empty($_FILES['postimage']['name']))
    		{
    			
    			$pimage = $this->input->post('opostimage');
    		}else{
    			
    			if (!$this->upload->do_upload('postimage')) {
    				$error = array('error' => $this->upload->display_errors());
    				$pimage = $this->input->post('opostimage');
    				
    			} else{
    				$postimage = array('upload_data' => $this->upload->data());
    				$pimage = $postimage['upload_data']['file_name'];
    			}
    		}
    		
    		$postslug = $this->create_unique_slug($this->input->post('postslug'), 'posts', 'post_slug', 'post_id', $pid);
    		
    		$pdata = array(
    				'category_id' => $this->input->post('categoryid'),
    				'post_title' => $this->input->post('postname'),
    				'post_slug' => $postslug,
    				'post_meta_title' => $this->input->post('postmetatitle'),
    				'post_meta_description' => $this->input->post('postmetadesc'),
    				'post_meta_keywords' => $this->input->post('postmetakeyword'),
    				'post_description'=> $this->input->post('postdesc'),
    				'post_image'=> $pimage,
    				'post_video'=> $this->input->post('postvideo'),
    				'post_image_alt' => $this->input->post('postimagealt'),
    				//'post_status' => 'a',
    				//'post_created' => date('Y-m-d H:i:s'),
    				'post_updated' => date('Y-m-d H:i:s'),
    		);
    		
    		$this->db->where('post_id', $pid);
    		$this->db->update('posts', $pdata);
    		$this->data['message'] = "Post Updated Successfully";
    	}
    	
    	$post = $this->posts_model->getPost($pid);
    	
    	$this->data['title'] = "Edit Post";
    	$this->data['view'] = 'managers/posts/edit_post_view';
    	$this->data['post'] = $post;
    	$this->template->managerTemplate($this->data);
    }

	public function posts(){

      
       $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');


        $config = array();
           $config["base_url"] = base_url()."managers/posts/posts";
           $total_row = $this->db->select('post_id')->from('posts')->where('post_status != ' , 't')->count_all_results();
           $config["total_rows"] = $total_row;
           $config["per_page"] = 10;
           $config['num_links'] = 8;
           //$config["uri_segment"] = 4;
           $config['page_query_string'] = TRUE;

           /* This Application Must Be Used With BootStrap 3 *  */
           $config['full_tag_open'] = '<ul class="pagination pagination-sm no-margin pull-right">';
           $config['full_tag_close'] ="</ul>";

           $config['num_tag_open'] = "<li>";
           $config['num_tag_close'] = '</li>';

           $config['cur_tag_open'] = '<li class="active"><a href="javascript:void();">';
           $config['cur_tag_close'] = "</a></li>";

           $config['next_tag_open'] = "<li>";
           $config['next_tagl_close'] = "</ul>";

           $config['prev_tag_open'] = "<li>";
           $config['prev_tagl_close'] = "</ul>";

           $config['first_tag_open'] = "<li>";
           $config['first_tagl_close'] = "</li>";

           $config['last_tag_open'] = "<li>";
           $config['last_tagl_close'] = "</li>";

           $this->pagination->initialize($config);
           //$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $page = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
           $inputs['page'] = $page;
           $inputs['per_page'] = $config['per_page'];

        $posts =  $this->posts_model->getPosts($inputs);
       
       
        $this->data['posts'] = $posts;
       
        $this->data['view'] = 'managers/posts/home_view';
        
        $this->template->managerTemplate($this->data);

    }
    
    public function operatorPosts($uid){
    	
    	
    	$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
    	
    	
    	$config = array();
    	$config["base_url"] = base_url()."managers/operatorPosts/".$uid;
    	$total_row = $this->db->select('post_id')->from('posts')->where('post_status != "t" AND user_id='.$uid)->count_all_results();
    	
    	$config["total_rows"] = $total_row;
    	$config["per_page"] = 10;
    	$config['num_links'] = 8;
    	//$config["uri_segment"] = 4;
    	$config['page_query_string'] = TRUE;
    	
    	/* This Application Must Be Used With BootStrap 3 *  */
    	$config['full_tag_open'] = '<ul class="pagination pagination-sm no-margin pull-right">';
    	$config['full_tag_close'] ="</ul>";
    	
    	$config['num_tag_open'] = "<li>";
    	$config['num_tag_close'] = '</li>';
    	
    	$config['cur_tag_open'] = '<li class="active"><a href="javascript:void();">';
    	$config['cur_tag_close'] = "</a></li>";
    	
    	$config['next_tag_open'] = "<li>";
    	$config['next_tagl_close'] = "</ul>";
    	
    	$config['prev_tag_open'] = "<li>";
    	$config['prev_tagl_close'] = "</ul>";
    	
    	$config['first_tag_open'] = "<li>";
    	$config['first_tagl_close'] = "</li>";
    	
    	$config['last_tag_open'] = "<li>";
    	$config['last_tagl_close'] = "</li>";
    	
    	$this->pagination->initialize($config);
    	//$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
    	$page = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
    	$inputs['page'] = $page;
    	$inputs['per_page'] = $config['per_page'];
    	$inputs['user_id'] = $uid;
    	$posts =  $this->posts_model->getOperatorPosts($inputs);
    	
    	
    	$this->data['posts'] = $posts;
    	
    	$this->data['view'] = 'managers/posts/operator_posts_view';
    	
    	$this->template->managerTemplate($this->data);
    	
    }
    public function trashPosts(){
      $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

            $config = array();
            $config["base_url"] = base_url()."managers/posts/trashPosts";
            $total_row = $this->db->select('post_id')->from('posts')->where('post_status', 't')->count_all_results();


            $config["total_rows"] = $total_row;
            $config["per_page"] = 10;
            $config['num_links'] = 4;
            //$config["uri_segment"] = 4;
            $config['page_query_string'] = TRUE;

            /* This Application Must Be Used With BootStrap 3 *  */
            $config['full_tag_open'] = '<ul class="pagination pagination-sm no-margin pull-right">';
            $config['full_tag_close'] ="</ul>";

            $config['num_tag_open'] = "<li>";
            $config['num_tag_close'] = '</li>';

            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void();">';
            $config['cur_tag_close'] = "</a></li>";

            $config['next_tag_open'] = "<li>";
            $config['next_tagl_close'] = "</ul>";

            $config['prev_tag_open'] = "<li>";
            $config['prev_tagl_close'] = "</ul>";

            $config['first_tag_open'] = "<li>";
            $config['first_tagl_close'] = "</li>";

            $config['last_tag_open'] = "<li>";
            $config['last_tagl_close'] = "</li>";

            $this->pagination->initialize($config);
            //$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            $page = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
            $inputs['page'] = $page;
            $inputs['per_page'] = $config['per_page'];

            $posts = $this->posts_model->get_trash_posts($inputs);
            $this->data['posts'] = $posts;


            $this->data['title'] = "";
            $this->data['view'] = 'managers/posts/trash_posts_view';
            $this->template->managerTemplate($this->data);

        
    }
    public function trashSearch(){

            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
            $pname = $this->input->get('pname');

            $config = array();
            $config["base_url"] = base_url()."managers/posts/trashSearch?pname=".$pname;

            $total_row = $this->db->select('post_id')->from('posts')->where('post_status', 't')->like('post_title' , $pname )->count_all_results();

            $config["total_rows"] = $total_row; 
            $config["per_page"] = 10;
            $config['num_links'] = 8;
            //$config["uri_segment"] = 4;
            $config['page_query_string'] = TRUE;

            /* This Application Must Be Used With BootStrap 3 *  */
            $config['full_tag_open'] = '<ul class="pagination pagination-sm no-margin pull-right">';
            $config['full_tag_close'] ="</ul>";

            $config['num_tag_open'] = "<li>";
            $config['num_tag_close'] = '</li>';

            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void();">';
            $config['cur_tag_close'] = "</a></li>";

            $config['next_tag_open'] = "<li>";
            $config['next_tagl_close'] = "</ul>";

            $config['prev_tag_open'] = "<li>";
            $config['prev_tagl_close'] = "</ul>";

            $config['first_tag_open'] = "<li>";
            $config['first_tagl_close'] = "</li>";

            $config['last_tag_open'] = "<li>";
            $config['last_tagl_close'] = "</li>";

            $this->pagination->initialize($config);
            //$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            $page = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
            $inputs['page'] = $page;
            $inputs['per_page'] = $config['per_page'];
            $inputs['pname'] = $pname;


            $this->db->select('*');
            $this->db->like('post_title' , $pname);
            //$this->db->limit($page, $config['per_page']);
            $posts = $this->db->get('posts',$config['per_page'], $page )->result_array();

            $this->data['posts'] = $posts;

            $this->data['pname'] = $pname;
           if (count($posts) > 0) {
              $this->data['message'] = "Search results for ".'"'.$pname.'"';
            }else{
               $this->data['message'] = " OMG! Results Not Found";
            }
            $this->data['title'] = "";
            $this->data['view'] = 'managers/posts/trash_posts_view';
            $this->template->managerTemplate($this->data);
    }

    public function deactivatePosts(){
      $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

            $config = array();
            $config["base_url"] = base_url()."managers/posts/deactivatePosts";
            $total_row = $this->db->select('post_id')->from('posts')->where('post_status', 't')->count_all_results();


            $config["total_rows"] = $total_row;
            $config["per_page"] = 10;
            $config['num_links'] = 4;
            //$config["uri_segment"] = 4;
            $config['page_query_string'] = TRUE;

            /* This Application Must Be Used With BootStrap 3 *  */
            $config['full_tag_open'] = '<ul class="pagination pagination-sm no-margin pull-right">';
            $config['full_tag_close'] ="</ul>";

            $config['num_tag_open'] = "<li>";
            $config['num_tag_close'] = '</li>';

            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void();">';
            $config['cur_tag_close'] = "</a></li>";

            $config['next_tag_open'] = "<li>";
            $config['next_tagl_close'] = "</ul>";

            $config['prev_tag_open'] = "<li>";
            $config['prev_tagl_close'] = "</ul>";

            $config['first_tag_open'] = "<li>";
            $config['first_tagl_close'] = "</li>";

            $config['last_tag_open'] = "<li>";
            $config['last_tagl_close'] = "</li>";

            $this->pagination->initialize($config);
            //$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            $page = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
            $inputs['page'] = $page;
            $inputs['per_page'] = $config['per_page'];

            $posts = $this->posts_model->get_deactivate_posts($inputs);
            $this->data['posts'] = $posts;


            $this->data['title'] = "";
            $this->data['view'] = 'managers/posts/deactivate_post_view';
            $this->template->managerTemplate($this->data);

        
    }



	public function search(){
    		$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
        

            $pname = $this->input->get('pname');

            $config = array();
            $config["base_url"] = base_url()."managers/posts/search?pname=".$pname;

            $total_row = $this->db->select('post_id')->from('posts')->like('post_title' , $pname )->count_all_results();

            $config["total_rows"] = $total_row;
            $config["per_page"] = 10;
            $config['num_links'] = 4;
            //$config["uri_segment"] = 4;
            $config['page_query_string'] = TRUE;

            /* This Application Must Be Used With BootStrap 3 *  */
            $config['full_tag_open'] = '<ul class="pagination pagination-sm no-margin pull-right">';
            $config['full_tag_close'] ="</ul>";

            $config['num_tag_open'] = "<li>";
            $config['num_tag_close'] = '</li>';

            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void();">';
            $config['cur_tag_close'] = "</a></li>";

            $config['next_tag_open'] = "<li>";
            $config['next_tagl_close'] = "</ul>";

            $config['prev_tag_open'] = "<li>";
            $config['prev_tagl_close'] = "</ul>";

            $config['first_tag_open'] = "<li>";
            $config['first_tagl_close'] = "</li>";

            $config['last_tag_open'] = "<li>";
            $config['last_tagl_close'] = "</li>";

            $this->pagination->initialize($config);
            //$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            $page = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
            $inputs['page'] = $page;
            $inputs['per_page'] = $config['per_page'];
            $inputs['pname'] = $pname;


            $this->db->select('*');
            $this->db->like('post_title' , $pname);
            $this->db->join('users', 'posts.user_id = users.id');
            //$this->db->limit($page, $config['per_page']);
            $posts = $this->db->get('posts',$config['per_page'], $page )->result_array();
  

            if (count($posts) > 0) {
              $this->data['message'] = "Search results for ".'"'.$pname.'"';
            }else{
               $this->data['message'] = " OMG! Results Not Found";
            }
            
            $this->data['posts'] = $posts;
            $this->data['pname'] = $pname;
            $this->data['title'] = "";
            $this->data['view'] = 'managers/posts/home_view';
            $this->template->managerTemplate($this->data);
    }
    
    public function activatePost($pid=null){
                if ($pid > 0) {
                $pdata = array(
                      'post_status' => 'a',
                      'post_updated' => date('Y-m-d H:i:s'),
                );
                $this->posts_model->activate_post($pdata, $pid);
                //$this->data['message'] = "Post ID: ". $pid . " Successfully activated";
        $this->session->set_flashdata('message', "Post ID: ". $pid . " Successfully activated");
                //$this->posts();

         redirect('managers/posts/posts', 'refresh');
                }

    }
    public function deactivatePost($pid=null){
        if ($pid > 0) {
        $pdata = array(
              'post_status' => 'd',
              'post_updated' => date('Y-m-d H:i:s'),
        );
        $this->posts_model->deactivate_post($pdata, $pid);
        // set the flash data error message if there is one
        //$this->data['message'] = "Post ID: ". $pid . " Successfully Deactivated";
    $this->session->set_flashdata('message', "Post ID: ". $pid . " Successfully Deactivated");
        //$this->posts();
     redirect('managers/posts/posts', 'refresh');
        }
    }

    public function trashPost($pid=null){
        if ($pid > 0) {
                $pdata = array(
                           'post_status' => 't',
                           'post_updated' => date('Y-m-d H:i:s'),
                        );
                $this->posts_model->activate_post($pdata, $pid);

                //$this->data['message'] = "Post ID: ". $pid . " Successfully Trashed";
                $this->session->set_flashdata('message', "Post ID: ". $pid . " Successfully Trashed");
                //$this->posts();
                 redirect('managers/posts/posts', 'refresh');
        }

    }



}