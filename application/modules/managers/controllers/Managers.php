<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Managers extends MY_Controller {
	public $data;
	function __construct()
	{
		parent::__construct();

		if ($this->ion_auth->logged_in())
		{
			if (!$this->ion_auth->in_group('managers'))
			{
				redirect(base_url().'members/login', 'refresh');
			}
		}else{
			redirect(base_url().'members/login', 'refresh');
		}
	}

	// redirect if needed, otherwise display the user list
	function index()
	{

		$posts = null;
		$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

		

		//list the users
		$this->data['users'] = $this->ion_auth->users()->result();
		foreach ($this->data['users'] as $k => $user)
		{
			$this->data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
		}
			$config = array();
           $config["base_url"] = base_url()."managers/";
           $total_row = $this->db->select('post_id')->from('posts')->where('post_status != ' , 't')->count_all_results();
           $config["total_rows"] = $total_row;
           $config["per_page"] = 10;
           $config['num_links'] = 8  ;
           //$config["uri_segment"] = 4;
           $config['page_query_string'] = TRUE;

           /* This Application Must Be Used With BootStrap 3 *  */
           $config['full_tag_open'] = '<ul class="pagination pagination-sm no-margin pull-right">';
           $config['full_tag_close'] ="</ul>";

           $config['num_tag_open'] = "<li>";
           $config['num_tag_close'] = '</li>';

           $config['cur_tag_open'] = '<li class="active"><a href="javascript:void();">';
           $config['cur_tag_close'] = "</a></li>";

           $config['next_tag_open'] = "<li>";
           $config['next_tagl_close'] = "</ul>";

           $config['prev_tag_open'] = "<li>";
           $config['prev_tagl_close'] = "</ul>";

           $config['first_tag_open'] = "<li>";
           $config['first_tagl_close'] = "</li>";

           $config['last_tag_open'] = "<li>";
           $config['last_tagl_close'] = "</li>";

           $this->pagination->initialize($config);
           //$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $page = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
           $inputs['page'] = $page;
           $inputs['per_page'] = $config['per_page'];


          $this->load->model('managers/posts_model');
          $posts =  $this->posts_model->getPosts($inputs);
             
             
          $data['posts'] = $posts;
          $data['message'] = "Logged In Successfully";
        	$data['view'] = 'managers/home_view';
        	$this->template->managerTemplate($data);
	}



	public function search(){
    $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

            $pname = $this->input->get('pname');

            $config = array();
            $config["base_url"] = base_url()."managers/search?pname=".$pname;

            $total_row = $this->db->select('post_id')->from('posts')->like('post_title' , $pname )->count_all_results();

            $config["total_rows"] = $total_row;
            $config["per_page"] = 10;
            $config['num_links'] = 4;
            //$config["uri_segment"] = 4;
            $config['page_query_string'] = TRUE;

            /* This Application Must Be Used With BootStrap 3 *  */
            $config['full_tag_open'] = '<ul class="pagination pagination-sm no-margin pull-right">';
            $config['full_tag_close'] ="</ul>";

            $config['num_tag_open'] = "<li>";
            $config['num_tag_close'] = '</li>';

            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void();">';
            $config['cur_tag_close'] = "</a></li>";

            $config['next_tag_open'] = "<li>";
            $config['next_tagl_close'] = "</ul>";

            $config['prev_tag_open'] = "<li>";
            $config['prev_tagl_close'] = "</ul>";

            $config['first_tag_open'] = "<li>";
            $config['first_tagl_close'] = "</li>";

            $config['last_tag_open'] = "<li>";
            $config['last_tagl_close'] = "</li>";

            $this->pagination->initialize($config);
            //$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            $page = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
            $inputs['page'] = $page;
            $inputs['per_page'] = $config['per_page'];
            $inputs['pname'] = $pname;

            $this->data['pname'] = $pname;


            $this->db->select('*');
           
            $this->db->like('post_title' , $pname);
            //$this->db->limit($page, $config['per_page']);
            $posts = $this->db->get('posts',$config['per_page'], $page )->result_array();
  

        
           


            $this->data['posts'] = $posts;

             $this->data['message'] = " Results Found ";
            $this->data['title'] = "";
            $this->data['view'] = 'managers/home_view';
            $this->template->managerTemplate($this->data);

        
    }
    


}
