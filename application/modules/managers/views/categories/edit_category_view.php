<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
    <section class="content-header">
           <h1>
             <?php echo $title; ?>
             <small><div id="infoMessage"><?php echo $message;?></div></small>
           </h1>
           <ol class="breadcrumb">
            <li><a href="<?php echo base_url()."managers/categories/add_category"; ?>">Add Category </a> </li>
              <li><a href="<?php echo base_url()."managers/categories/categories"; ?>">All Categories</a> </li> 
            
            
           </ol>
         </section>

 <!-- Main content -->
         <section class="content">



         <div class="row">
           <div class="col-md-6 col-md-offset-3">
              <!-- Default box -->
           <div class="box">
             <div class="box-header with-border">
               <h3 class="box-title">Please enter category details</h3>
               <div class="box-tools pull-right">
                   
                    
               <!--   <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                 <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button> -->
               </div>
             </div>
             <div class="box-body">
 <?php echo validation_errors(); ?>

<?php echo form_open("managers/categories/edit_category/".$category['category_id'], array('role' => 'form'));?>

      <p>
      <label for="Category Name"> Category Name</label>
      <input type="text" name="categoryname" id="categoryname" class="form-control" value="<?php echo $category['category_name']; ?>" />
      <input type="submit" name="submit" id="submit" value="Submit" class="" /> 
      </p>




<?php echo form_close();?>

             </div><!-- /.box-body -->
              
          </div><!-- /.box -->

           </div>    
         </div>
           







      </section><!-- /.content -->