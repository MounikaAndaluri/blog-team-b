 <?php defined('BASEPATH') OR exit('No direct script access allowed');?>
 <!-- Content Header (Page header) -->

         <section class="content-header">
            <h1>
             Operator Related Posts 
             <small><div id="infoMessage"><?php echo $message;?></div></small>
           </h1> 
           <ol class="breadcrumb">
             <li><a href="<?php echo base_url()."managers/posts/add_post"; ?>">Add Post</a> </li>
            <li><a href="<?php echo base_url()."managers/posts/posts"; ?>">All Post</a> </li>
           </ol>
         </section>

         <!-- Main content -->
         <section class="content">

           <!-- Default box -->
           <div class="box">
             <div class="box-header with-border">
                <?php echo form_open("managers/posts/search", array('role' => 'form', 'method' => 'get'));?>
                <?php $pname = isset($pname) ? $pname : ""?>
                  <input type="text" name="pname" value="<?php echo $pname; ?>" placeholder="Post Name" />
                  <input type="submit" name="psubmit" value="Search" />
               
                <?php echo form_close();?>
               <div class="box-tools pull-right">
                     
               <?php echo $this->pagination->create_links(); ?>
                    
               <!--   <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                 <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button> -->
               </div>
             </div>
             <div class="box-body">
                
                 <table class="table">
                  <tr>
                    <th>Post id</th>
                    <th>User Name</th>  
                    <th>Post Name</th> 
                   <!--  <th>Post Description</th> -->
                    <th>Post Image</th>
                    <th>Post Status</th>
                   
                    <th>Update</th>

                  </tr>
                  <?php //echo "<pre>"; print_r($posts); echo "</pre>"; ?>
                  <?php foreach ($posts as $pst) { ?>

                    <tr>
                        <td><?php echo $pst['post_id']; ?></td> 
                        <td><?php echo $pst['first_name']." ".$pst['last_name']; ?></td>     
                        <td><?php echo $pst['post_title']; ?></td> 
                        <!-- <td><?php //echo $pst['post_description']; ?></td> -->
                        <td><img src="<?php echo base_url()."images/posts/".$pst['post_image']; ?>" alt="" width="50" height="50" /></td>
                       <td>
                              <?php 
                                if ($pst['post_status'] == 'a') {
                                    echo 'active | <a href="'.base_url().'managers/posts/deactivatePost/'.$pst['post_id'].'">deactive</a> ';
                                }elseif ($pst['post_status'] == 'd') {
                                  echo '<a href="'.base_url().'managers/posts/activatePost/'.$pst['post_id'].'">active</a> | deactive ';
                                } 
                             ?>
       
                        </td>   
                        <td><a href="<?php echo base_url()."managers/posts/edit_Post/".$pst['post_id']; ?>">Update</a></td>
                        <!-- <td><a href="<?php //echo base_url()."managers/posts/delete_post/".$pst['post_id']; ?>">Delete</a></td> -->
                    </tr>
  
                  <?php } ?>
                  
                  </table>
             
             </div><!-- /.box-body -->
             
           </div><!-- /.box -->

         </section><!-- /.content -->