<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

	  function __construct(){
	     parent::__construct();
	     // we have to load the blog categories  // menu purpose
	  }

	public function index()
	{
		$data = null;
		
		$posts = null;
		$this->load->model('blog/blogs_model');

		$config = array();
        $config["base_url"] = base_url()."blog/posts/";
        $total_row = $this->db->select('post_id')->where('post_status', '1')->from('posts')->count_all_results();

        $config["total_rows"] = $total_row;
        $config["per_page"] = 10;
        $config['num_links'] = 15;
        $config["uri_segment"] = 4;

        /* This Application Must Be Used With BootStrap 3 *  */
        $config['full_tag_open'] = "<ul class='pagination pagination-flat pagination-separated-np'>";
        $config['full_tag_close'] ="</ul>";

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = "</a></li>";

        $config['next_tag_open'] = '<li>';

        $config['next_tagl_close'] = "</li>";

        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";

        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;


		$posts = $this->blogs_model->getPosts($page, $config['per_page']);

		$categories = $this->blogs_model->getCategories();
		 
		$data['categories']=$categories;
		$data['posts'] = $posts;
		
		$data['view'] = 'home/home_view';
    	$this->template->HomeTemplate($data);
	}

	public function about(){
	    $data = null;

	    $data['view'] = 'home/about_view';
	    $this->template->HomeTemplate($data);
	}

	public function contact(){
	    $data = null;

	    $data['view'] = 'home/contact_view';
	    $this->template->HomeTemplate($data);
	}


	public function termsandconditions(){

 $data = null;

	    $data['view'] = 'home/termsandconditions_view';
	    $this->template->HomeTemplate($data);


		
	}


	
}
