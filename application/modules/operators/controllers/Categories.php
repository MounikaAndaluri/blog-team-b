<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends MY_Controller {
	public $data;
	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url','language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('admin');
		$this->load->model('operators/categories_model');

		if ($this->ion_auth->logged_in())
		{
			if (!$this->ion_auth->in_group('operator'))
			{
				redirect(base_url().'members/login', 'refresh');
			}
		}else{
			redirect(base_url().'members/login', 'refresh');
		}
	}



	public function index() {


		// set the flash data error message if there is one
		$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
		
		$config = array();
           $config["base_url"] = base_url()."operators/categories";
           $total_row = $this->db->select('Category_id')->from('categories')->where('category_status !=','t')->count_all_results();
           $config["total_rows"] = $total_row;
           $config["per_page"] = 30;
           $config['num_links'] = 8;
           //$config["uri_segment"] = 4;
           $config['page_query_string'] = TRUE;

           /* This Application Must Be Used With BootStrap 3 *  */
           $config['full_tag_open'] = '<ul class="pagination pagination-sm no-margin pull-right">';
           $config['full_tag_close'] ="</ul>";

           $config['num_tag_open'] = "<li>";
           $config['num_tag_close'] = '</li>';

           $config['cur_tag_open'] = '<li class="active"><a href="javascript:void();">';
           $config['cur_tag_close'] = "</a></li>";

           $config['next_tag_open'] = "<li>";
           $config['next_tagl_close'] = "</ul>";

           $config['prev_tag_open'] = "<li>";
           $config['prev_tagl_close'] = "</ul>";

           $config['first_tag_open'] = "<li>";
           $config['first_tagl_close'] = "</li>";

           $config['last_tag_open'] = "<li>";
           $config['last_tagl_close'] = "</li>";

           $this->pagination->initialize($config);
           //$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $page = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
           $inputs['page'] = $page;
           $inputs['per_page'] = $config['per_page'];

    $categories =  $this->categories_model->getCategories($inputs);
     
     
    $this->data['categories'] = $categories;
    /*print_r($bloodbanks);
    exit();*/

		$this->data['title'] = "Categories";
		$this->data['view'] = 'operators/categories/home_view';
		$this->data['categories'] = $categories;


		$this->template->operatorTemplate($this->data);
		//$this->template->homeTemplate($this->data);
	}

	public function getCategoriesSugg() {
		$postvars = $this->input->get();
			try{
				$institutes = $this->categories_model->getCategoriesSugg($postvars);
			}catch(Exception $e) {
				echo $e->getMessage();
				$institutes = null;
			}

			$jsonins = json_encode($institutes);
			echo $jsonins;
	}
	
	
	
	public function search(){
      $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

            $cname = $this->input->get('cname');

            $config = array();
            $config["base_url"] = base_url()."operators/categories/search?cname=".$cname;

            $total_row = $this->db->select('category_id')->from('categories')->like('category_name' , $cname )->count_all_results();

            $config["total_rows"] = $total_row;
            $config["per_page"] = 30;
            $config['num_links'] = 4;
            //$config["uri_segment"] = 4;
            $config['page_query_string'] = TRUE;

            /* This Application Must Be Used With BootStrap 3 *  */
            $config['full_tag_open'] = '<ul class="pagination pagination-sm no-margin pull-right">';
            $config['full_tag_close'] ="</ul>";

            $config['num_tag_open'] = "<li>";
            $config['num_tag_close'] = '</li>';

            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void();">';
            $config['cur_tag_close'] = "</a></li>";

            $config['next_tag_open'] = "<li>";
            $config['next_tagl_close'] = "</ul>";

            $config['prev_tag_open'] = "<li>";
            $config['prev_tagl_close'] = "</ul>";

            $config['first_tag_open'] = "<li>";
            $config['first_tagl_close'] = "</li>";

            $config['last_tag_open'] = "<li>";
            $config['last_tagl_close'] = "</li>";

            $this->pagination->initialize($config);
            //$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            $page = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
            $inputs['page'] = $page;
            $inputs['per_page'] = $config['per_page'];
            $inputs['cname'] = $cname;


            $this->db->select('*');
          
            $this->db->like('category_name' , $cname);
            //$this->db->limit($page, $config['per_page']);
            $categories = $this->db->get('categories',$config['per_page'], $page )->result_array();
  

           
            if (count($categories) > 0) {
              $this->data['message'] = "Search results for ".'"'.$cname.'"';
            }else{
               $this->data['message'] = " OMG! Results Not Found";
            }
            $this->data['categories'] = $categories;

            $this->data['cname'] = $cname;
            $this->data['title'] = "";
            $this->data['view'] = 'operators/categories/home_view';
            $this->template->operatorTemplate($this->data);

    }

	public function categories(){
    $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

		$config = array();
           $config["base_url"] = base_url()."operators/categories/categories";
           $total_row = $this->db->select('Category_id')->from('categories')->where('category_status !=','t')->count_all_results();
           $config["total_rows"] = $total_row;
           $config["per_page"] = 30;
           $config['num_links'] = 8;
           //$config["uri_segment"] = 4;
           $config['page_query_string'] = TRUE;

           /* This Application Must Be Used With BootStrap 3 *  */
           $config['full_tag_open'] = '<ul class="pagination pagination-sm no-margin pull-right">';
           $config['full_tag_close'] ="</ul>";

           $config['num_tag_open'] = "<li>";
           $config['num_tag_close'] = '</li>';

           $config['cur_tag_open'] = '<li class="active"><a href="javascript:void();">';
           $config['cur_tag_close'] = "</a></li>";

           $config['next_tag_open'] = "<li>";
           $config['next_tagl_close'] = "</ul>";

           $config['prev_tag_open'] = "<li>";
           $config['prev_tagl_close'] = "</ul>";

           $config['first_tag_open'] = "<li>";
           $config['first_tagl_close'] = "</li>";

           $config['last_tag_open'] = "<li>";
           $config['last_tagl_close'] = "</li>";

           $this->pagination->initialize($config);
           //$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $page = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
           $inputs['page'] = $page;
           $inputs['per_page'] = $config['per_page'];

		$categories =  $this->categories_model->getCategories($inputs);
	   
	   
		$this->data['categories'] = $categories;
	


		$this->data['view'] = 'operators/categories/home_view';
		
		$this->template->operatorTemplate($this->data);

	}



 


}