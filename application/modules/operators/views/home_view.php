<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
   Operators Dashboard
    <small><div id="infoMessage"><?php echo $message; ?></div></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Operators</a></li>
    <li class="active">Dashboard</li>
  </ol>
</section>






          <section class="content ">
                <div class="row">
                  <div class="col-md-12">
                    
                    
              <div class="box box-info ">
                <div class="box-header with-border">
                  
                  <div ><h3 class="box-title" >
             Latest Posts
             </h3> 
                   <?php echo form_open("operators/posts/search", array('role' => 'form', 'method' => 'get'));?>
                  <?php $pname = isset($pname) ? $pname : ""; ?>
                  <input type="text" name="pname" value="<?php echo $pname; ?>" placeholder="Post Name" required />
                  <input type="submit" name="psubmit" value="Search" />
               
                <?php echo form_close();?>
                </div>
                  <div class="box-tools pull-right" >
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <div class="table-responsive">
                    <table class="table no-margin">
                      <thead>
                        <tr>
                          <th>Post ID</th>
                          <th>Category Name</th>
                          <th>Post Name</th>
                          

                          
                        </tr>
                      </thead>

                      <tbody>
                      <?php foreach($posts as $p){ ?>
                        <tr>
                          <td><a href=""><?php echo $p['post_id']; ?></a></td>
                          <td><?php echo $p['category_name']; ?></td>
                          <td><?php echo $p['post_title']; ?></td>
                            
                        </tr>
                        
                        <?php }?>
                      
                     
                      </tbody>
                    </table>
                  </div><!-- /.table-responsive -->
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
                  <a href="<?php echo base_url()."operators/posts/add_post"?>" class="btn btn-sm btn-info btn-flat pull-left">Place New Post</a>
                  <a href="<?php echo base_url()."operators/posts"?>" class="btn btn-sm btn-default btn-flat pull-right">View All Posts</a>
                </div><!-- /.box-footer -->

                   <?php echo $this->pagination->create_links(); ?>
              </div><!-- /.box -->


 </div>
                </div>
              </section>
