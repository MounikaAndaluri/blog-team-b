<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
    <section class="content-header">
           <h1>
             <?php echo $title; ?>
             <small><div id="infoMessage"><?php echo $message;?></div></small>
           </h1>
           <ol class="breadcrumb">
               <li><a href="<?php echo base_url()."operators/posts/add_post"; ?>">Add Post </a> </li>
              <li><a href="<?php echo base_url()."operators/posts/posts"; ?>">All Posts</a> </li>      
           </ol>
         </section>

 <!-- Main content -->
         <section class="content">



         <div class="row">
           <div class="col-md-8 col-md-offset-2">
              <!-- Default box -->
           <div class="box">
             <div class="box-header with-border">
               <h3 class="box-title"></h3>
               <div class="box-tools pull-right">
                   
                    
               <!--   <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                 <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button> -->
               </div>
             </div>
             <div class="box-body">
 <?php echo validation_errors(); ?>

<?php echo form_open("operators/posts/edit_post/".$post['post_id'], array('role' => 'form', 'enctype' => 'multipart/form-data'));?>

    <div class="form-group">
        <label for="categories">categories</label>
        <input type="text" name="categories" id="categories" class="form-control" value="<?php echo $post['category_name']; ?>" />
        <input type="hidden" name="categoryid" id="categoryid" value="<?php echo $post['category_id']; ?>" />
    </div>

    <div class="form-group">
      <label for="post Name"> post Name</label>
      <input type="text" name="postname" id="postname" class="form-control" value="<?php echo $post['post_title']; ?>" />
    </div>

    <div class="form-group">
      <label for="post Slug"> post slug</label>
      <input type="text" name="postslug" id="postslug" class="form-control" value="<?php echo $post['post_slug']; ?>" />
    </div>

     <div class="form-group">
      <label for="post meta title"> post meta title</label>
      <input type="text" name="postmetatitle" id="postmetatitle" class="form-control" value="<?php echo $post['post_meta_title']; ?>" />
    </div>
    
    <div class="form-group">
      <label for="post meta description"> post meta description</label>
      <input type="text" name="postmetadesc" id="postmetadesc" class="form-control" value="<?php echo $post['post_meta_description']; ?>" />
    </div>
    <div class="form-group">
      <label for="post meta keyword"> post meta keywords</label>
      <input type="text" name="postmetakeyword" id="postmetakeyword" class="form-control" value="<?php echo $post['post_meta_keywords']; ?>" />
    </div>

     <div class="form-group">
        <label for="post Name"> Post Description</label>
      
          <textarea name="postdesc" id="postdesc" class="form-control html5editor" /><?php echo $post['post_description']; ?></textarea>  
      </div>
      <div class="form-group">
        <label for="post Name"> Post Image</label>
      <img src="<?php echo base_url()."images/posts/".$post['post_image']; ?>" width=100px; />
          <input type="file" name="postimage" id="postimage" class="form-control" />

      </div>
      <div class="form-group">
      <label for="post meta keyword"> post image alt</label>
      <input type="text" name="postimagealt" id="postimagealt" class="form-control" value="<?php echo $post['post_image_alt']; ?>" />
    </div>
      
      <div class="form-group">
          <label for="post Name"> Post Video Link(i.e youtube)</label>
          <input type="text" name="postvideo" id="postvideo" class="form-control" value="<?php echo $post['post_video']; ?>"/>
      </div>
      
      
      <input type="hidden" name="opostimage" value="<?php echo $post['post_image']; ?>" />
      <input type="submit" name="submit" id="submit" value="update" class="form-control btn btn-primary" />

      </p>


<?php echo form_close();?>

             </div><!-- /.box-body -->
              
          </div><!-- /.box -->

           </div>    
         </div>
           







      </section><!-- /.content -->