<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends MY_Controller {
	public $data;
	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url','language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('admin');
		$this->load->model('admin/categories_model');

		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('members/login', 'refresh');
		} elseif (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
			redirect('members/logout', 'refresh');
		}
	}



	public function index() {


		// set the flash data error message if there is one
		$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
		
		       $config = array();
           $config["base_url"] = base_url()."admin/categories";
           $total_row = $this->db->select('Category_id')->from('categories')->where('category_status !=','t')->count_all_results();
           $config["total_rows"] = $total_row;
           $config["per_page"] = 30;
           $config['num_links'] = 8;
           //$config["uri_segment"] = 4;
           $config['page_query_string'] = TRUE;

           /* This Application Must Be Used With BootStrap 3 *  */
           $config['full_tag_open'] = '<ul class="pagination pagination-sm no-margin pull-right">';
           $config['full_tag_close'] ="</ul>";

           $config['num_tag_open'] = "<li>";
           $config['num_tag_close'] = '</li>';

           $config['cur_tag_open'] = '<li class="active"><a href="javascript:void();">';
           $config['cur_tag_close'] = "</a></li>";

           $config['next_tag_open'] = "<li>";
           $config['next_tagl_close'] = "</ul>";

           $config['prev_tag_open'] = "<li>";
           $config['prev_tagl_close'] = "</ul>";

           $config['first_tag_open'] = "<li>";
           $config['first_tagl_close'] = "</li>";

           $config['last_tag_open'] = "<li>";
           $config['last_tagl_close'] = "</li>";

           $this->pagination->initialize($config);
           //$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $page = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
           $inputs['page'] = $page;
           $inputs['per_page'] = $config['per_page'];

    $categories =  $this->categories_model->getCategories($inputs);
     
     
    $this->data['categories'] = $categories;
    /*print_r($Categorys);
    exit();*/
		


		$this->data['title'] = "Categories";
		$this->data['view'] = 'admin/categories/home_view';
		$this->data['categories'] = $categories;
		$this->template->adminTemplate($this->data);
		//$this->template->homeTemplate($this->data);
	}

	public function getCategoriesSugg() {
		$postvars = $this->input->get();
			try{
				$institutes = $this->categories_model->getCategoriesSugg($postvars);
			}catch(Exception $e) {
				echo $e->getMessage();
				$institutes = null;
			}

			$jsonins = json_encode($institutes);
			echo $jsonins;
	}
	public function add_category(){
		// set the flash data error message if there is one
		$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
		
		 $config = array(
                array(
                        'field' => 'categoryname', 
                        'label' => 'Category name',
                        'rules' => 'required|is_unique[categories.category_name]',
                    ),
            ); 
        $this->form_validation->set_rules($config);

        //$this->form_validation->set_rules('categoryname', 'categoryname', 'required|is_unique[categories.category_name]');

        if ($this->form_validation->run() === TRUE)
        {
    			$cdata = array(
    			       'category_name' => $this->input->post('categoryname'),
    			       'category_status' => 'a',
    			       'category_created' => date('Y-m-d h:i:s'),
    			       'category_updated' => date('Y-m-d h:i:s'),
    			);

    			$this->db->insert('categories', $cdata);
    			$this->data['message'] = "category add succ";
        }

		$this->data['title'] = "Add Category";
		$this->data['view'] = 'admin/categories/add_category_view';
		$this->template->adminTemplate($this->data);
	}

	public function edit_category($cid=null){
		// set the flash data error message if there is one
		$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');		

		$config = array(
            array(
                    'field' => 'categoryname', 
                    'label' => 'Category name',
                    'rules' => 'required|edit_unique[categories.category_name.category_id.'.$cid.']',
                ),
         
        );

        $this->form_validation->set_rules($config);
 		if ($this->form_validation->run() === TRUE){
					$cdata = array(
					      'category_name' => $this->input->post('categoryname'),
					      'category_status' => 'a',
					      'category_created' => date('Y-m-d h:i:s'),
					      'category_updated' => date('Y-m-d h:i:s'),
					);
					$this->db->where('category_id', $cid);
					$this->db->update('categories', $cdata); 
					$this->data['message'] = "category updated successfully";
		}

		$category = $this->categories_model->getCategory($cid);

		$this->data['title'] = "Edit Category";
		$this->data['view'] = 'admin/categories/edit_category_view';
		$this->data['category'] = $category;
		$this->template->adminTemplate($this->data);
	}


	public function search(){
            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
            $cname = $this->input->get('cname');

            $config = array();
            $config["base_url"] = base_url()."admin/categories/search?cname=".$cname;

            $total_row = $this->db->select('category_id')->from('categories')->like('category_name' , $cname )->count_all_results();

            $config["total_rows"] = $total_row;
            $config["per_page"] = 30;
            $config['num_links'] = 4;
            //$config["uri_segment"] = 4;
            $config['page_query_string'] = TRUE;

            /* This Application Must Be Used With BootStrap 3 *  */
            $config['full_tag_open'] = '<ul class="pagination pagination-sm no-margin pull-right">';
            $config['full_tag_close'] ="</ul>";

            $config['num_tag_open'] = "<li>";
            $config['num_tag_close'] = '</li>';

            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void();">';
            $config['cur_tag_close'] = "</a></li>";

            $config['next_tag_open'] = "<li>";
            $config['next_tagl_close'] = "</ul>";

            $config['prev_tag_open'] = "<li>";
            $config['prev_tagl_close'] = "</ul>";

            $config['first_tag_open'] = "<li>";
            $config['first_tagl_close'] = "</li>";

            $config['last_tag_open'] = "<li>";
            $config['last_tagl_close'] = "</li>";

            $this->pagination->initialize($config);
            //$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            $page = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
            $inputs['page'] = $page;
            $inputs['per_page'] = $config['per_page'];
            $inputs['cname'] = $cname;


            $this->db->select('*');
           
            $this->db->like('category_name' , $cname);
            //$this->db->limit($page, $config['per_page']);
            $categories = $this->db->get('categories',$config['per_page'], $page )->result_array();
  

          

            $this->data['categories'] = $categories;

           
            if (count($categories) > 0) {
              $this->data['message'] = "Search results for ".'"'.$cname.'"';
            }else{
               $this->data['message'] = " OMG! Results Not Found";
            }
             $this->data['cname'] = $cname;
            $this->data['title'] = "";
            $this->data['view'] = 'admin/categories/home_view';
            $this->template->adminTemplate($this->data);

        
    }

      public function trashSearch(){

            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
            $cname = $this->input->get('cname');

            $config = array();
            $config["base_url"] = base_url()."admin/categories/trashSearch?cname=".$cname;

            $total_row = $this->db->select('category_id')->from('categories')->where('category_status', 't')->like('category_name' , $cname )->count_all_results();

            $config["total_rows"] = $total_row; 
            $config["per_page"] = 30;
            $config['num_links'] = 4;
            //$config["uri_segment"] = 4;
            $config['page_query_string'] = TRUE;

            /* This Application Must Be Used With BootStrap 3 *  */
            $config['full_tag_open'] = '<ul class="pagination pagination-sm no-margin pull-right">';
            $config['full_tag_close'] ="</ul>";

            $config['num_tag_open'] = "<li>";
            $config['num_tag_close'] = '</li>';

            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void();">';
            $config['cur_tag_close'] = "</a></li>";

            $config['next_tag_open'] = "<li>";
            $config['next_tagl_close'] = "</ul>";

            $config['prev_tag_open'] = "<li>";
            $config['prev_tagl_close'] = "</ul>";

            $config['first_tag_open'] = "<li>";
            $config['first_tagl_close'] = "</li>";

            $config['last_tag_open'] = "<li>";
            $config['last_tagl_close'] = "</li>";

            $this->pagination->initialize($config);
            //$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            $page = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
            $inputs['page'] = $page;
            $inputs['per_page'] = $config['per_page'];
            $inputs['cname'] = $cname;


            $this->db->select('*');
            $this->db->like('category_name' , $cname);
            //$this->db->limit($page, $config['per_page']);
            $categories = $this->db->get('categories',$config['per_page'], $page )->result_array();

            $this->data['categories'] = $categories;

            $this->data['cname'] = $cname;
           if (count($categories) > 0) {
              $this->data['message'] = "Search results for ".'"'.$cname.'"';
            }else{
               $this->data['message'] = " OMG! Results Not Found";
            }
            $this->data['title'] = "";
            $this->data['view'] = 'admin/categories/trash_categories_view';
            $this->template->adminTemplate($this->data);
    }

	public function categories(){

    $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

		$config = array();
           $config["base_url"] = base_url()."admin/categories/categories";
           $total_row = $this->db->select('Category_id')->from('categories')->where('category_status !=','t')->count_all_results();
           $config["total_rows"] = $total_row;
           $config["per_page"] = 30;
           $config['num_links'] = 8;
           //$config["uri_segment"] = 4;
           $config['page_query_string'] = TRUE;

           /* This Application Must Be Used With BootStrap 3 *  */
           $config['full_tag_open'] = '<ul class="pagination pagination-sm no-margin pull-right">';
           $config['full_tag_close'] ="</ul>";

           $config['num_tag_open'] = "<li>";
           $config['num_tag_close'] = '</li>';

           $config['cur_tag_open'] = '<li class="active"><a href="javascript:void();">';
           $config['cur_tag_close'] = "</a></li>";

           $config['next_tag_open'] = "<li>";
           $config['next_tagl_close'] = "</ul>";

           $config['prev_tag_open'] = "<li>";
           $config['prev_tagl_close'] = "</ul>";

           $config['first_tag_open'] = "<li>";
           $config['first_tagl_close'] = "</li>";

           $config['last_tag_open'] = "<li>";
           $config['last_tagl_close'] = "</li>";

           $this->pagination->initialize($config);
           //$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $page = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
           $inputs['page'] = $page;
           $inputs['per_page'] = $config['per_page'];

		$categories =  $this->categories_model->getCategories($inputs);
	   
	   
		$this->data['categories'] = $categories;
		


		$this->data['view'] = 'admin/categories/home_view';
		
		$this->template->adminTemplate($this->data);

	}
	public function trashCategories(){
     $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

            $config = array();
            $config["base_url"] = base_url()."admin/categories/trashCategories";
            $total_row = $this->db->select('Category_id')->from('categories')->where('Category_status', 't')->count_all_results();


            $config["total_rows"] = $total_row;
            $config["per_page"] = 30;
            $config['num_links'] = 4;
            //$config["uri_segment"] = 4;
            $config['page_query_string'] = TRUE;

            /* This Application Must Be Used With BootStrap 3 *  */
            $config['full_tag_open'] = '<ul class="pagination pagination-sm no-margin pull-right">';
            $config['full_tag_close'] ="</ul>";

            $config['num_tag_open'] = "<li>";
            $config['num_tag_close'] = '</li>';

            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void();">';
            $config['cur_tag_close'] = "</a></li>";

            $config['next_tag_open'] = "<li>";
            $config['next_tagl_close'] = "</ul>";

            $config['prev_tag_open'] = "<li>";
            $config['prev_tagl_close'] = "</ul>";

            $config['first_tag_open'] = "<li>";
            $config['first_tagl_close'] = "</li>";

            $config['last_tag_open'] = "<li>";
            $config['last_tagl_close'] = "</li>";

            $this->pagination->initialize($config);
            //$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            $page = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
            $inputs['page'] = $page;
            $inputs['per_page'] = $config['per_page'];

            $categories = $this->categories_model->get_trash_categories($inputs);
            $this->data['categories'] = $categories;

              //$this->data['message'] = "";
            $this->data['title'] = "";
            $this->data['view'] = 'admin/categories/trash_categories_view';
            $this->template->adminTemplate($this->data);
  }

  public function trashCategory($cid=null){
        if ($cid > 0) {
                $cdata = array(
                           'category_status' => 't',
                           'category_updated' => date('Y-m-d h:i:s'),
                        );
                $this->categories_model->activate_category($cdata, $cid);
                $this->session->set_flashdata('message', "Category ID: ". $cid . " Successfully Trashed");
                //$this->categories();
                 redirect('admin/categories/categories', 'refresh');

        }

  }

	public function activateCategory($cid=null){
				if ($cid > 0) {
				$cdata = array(
				      'category_status' => 'a',
				      'category_updated' => date('Y-m-d h:i:s'),
				);
				$this->categories_model->activate_category($cdata, $cid);
				$this->session->set_flashdata('message', "Category ID: ". $cid . " Successfully Activated");
				//$this->categories();
         redirect('admin/categories/categories', 'refresh');
				}
	}

	public function deactivateCategory($cid=null){
		if ($cid > 0) {
		$cdata = array(
		      'category_status' => 'd',
		      'category_updated' => date('Y-m-d h:i:s'),
		);
		$this->categories_model->deactivate_category($cdata, $cid);
		// set the flash data error message if there is one
		$this->session->set_flashdata('message', "Category ID: ". $cid . " Successfully Deactivated");
		//$this->categories();
     redirect('admin/categories/categories', 'refresh');
		}
	}

	public function deleteCategory($cid=null){
		if($cid>0) {

			$this->categories_model->delete_category($cid);
			$this->session->set_flashdata('message', "Category ID: ". $cid . " Successfully Deleted");
			//$this->index();
       redirect('admin/categories/trashCategories', 'refresh');
		}
	}



}