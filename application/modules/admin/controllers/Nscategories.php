<?php defined('BASEPATH') OR exit('No direct script access allowed');
class nscategories extends MY_Controller {
	public $data;

	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url','language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('admin');
		$this->load->model('admin/ns_categories_model');

		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('members/login', 'refresh');
		} elseif (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
			redirect('members/logout', 'refresh');
		}
	}

	public function index(){
	
		$categories = $this->ns_categories_model->all_categories();


        $this->data['categories'] = $categories;
		$this->data['title'] = "Add NS Category";
		$this->data['view'] = 'admin/nscategories/home_view';
		$this->template->adminTemplate($this->data);			
	}

	public function createCategory(){

		// set the flash data error message if there is one
		$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

		$config = array(
	                array(
	                        'field' => 'categoryname', 
	                        'label' => 'Category name',
	                        'rules' => 'required',
	                    ),
        			);
        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() === TRUE)
        {
	        	$cimage = "";
	        	$img_config = null;
				$img_config['upload_path'] = APPPATH.'../images/categories/';
				$img_config['allowed_types'] = '*';
				$img_config['encrypt_name'] = TRUE;
				
				$this->load->library('upload', $img_config);

				if (!$this->upload->do_upload('categoryimage')) {
							$error = array('error' => $this->upload->display_errors());
							//print_r($error);
							//exit();
				} else{
							$categoryimage = array('upload_data' => $this->upload->data()); // returns uploaded image data as an array
							$cimage = $categoryimage['upload_data']['file_name'];
				}

        		$parent_cat_id = $this->input->post('parent_cat_id');
        		$category_name = $this->input->post('categoryname');
        		$cat_icon = $this->input->post('categoryicon');
        		$categoryslug = rawurlencode(str_replace(' ', '-', strtolower($category_name)));

				$cat_id = $this->ns_categories_model->add($parent_cat_id, $category_name);

				$c_array = array(
							'ns_category_slug' => $categoryslug,
							'ns_category_image' => $cimage,
							'ns_category_icon' => $cat_icon,
							'ns_category_status' => 'a',
							'ns_category_created' => date('Y-m-d h:i:s'),
							'ns_category_updated' => date('Y-m-d h:i:s'),
							);

			$this->db->where('ns_category_id', $cat_id);
			$this->db->update('ns_categories', $c_array);
			$this->data['message'] = " Category ".$cat_id." Added Sucessfully ";
        }

        $categories = $this->ns_categories_model->all_categories();
        $this->data['categories'] = $categories;
		$this->data['title'] = "Add NS Category";
		$this->data['view'] = 'admin/nscategories/add_nscategory_view';
		$this->template->adminTemplate($this->data);

	}

	public function updateCategory($cid){
		//set the flash data error message if there is one
		$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

		$config = array(
	                array(
	                        'field' => 'categoryname', 
	                        'label' => 'Category name',
	                        'rules' => 'required',
	                    ),
        			);

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() === TRUE)
        {
        	$cimage = "";
        	$img_config = null;
			$img_config['upload_path'] = APPPATH.'../images/categories/';
			$img_config['allowed_types'] = '*';
			$img_config['encrypt_name'] = TRUE;
			
			$this->load->library('upload', $img_config);

			if (empty($_FILES['categoryimage']['name']))
			{
					$cimage = $this->input->post('ocategoryimage');
			}else{
				if (!$this->upload->do_upload('categoryimage')) {
					$error = array('error' => $this->upload->display_errors());
					//print_r($error);
					//exit();
				} else{
					$categoryimage = array('upload_data' => $this->upload->data()); // returns uploaded image data as an array
					$cimage = $categoryimage['upload_data']['file_name'];
				}
			}

    		$parent_cat_id = $this->input->post('parent_cat_id');
    		$category_name = $this->input->post('categoryname');
    		$cat_icon = $this->input->post('categoryicon');
    		$categoryslug = rawurlencode(str_replace(' ', '-', strtolower($category_name)));

    		$source = $cid;
    		$target = $parent_cat_id;

    		$this->ns_categories_model->move($source, $target );

			//$this->ns_categories_model->update($cid, $category_name);
			$c_array = array(
						'ns_category_name' => $category_name,
						'ns_category_slug' => $categoryslug,
						'ns_category_image' => $cimage,
						'ns_category_icon' => $cat_icon,

						//'ns_category_status' => 'a',
						//'ns_category_created' => date('Y-m-d h:i:s'),
						'ns_category_updated' => date('Y-m-d h:i:s'),
						);

			$this->db->where('ns_category_id', $cid);
			$this->db->update('ns_categories', $c_array);
			$this->data['message'] = " Category ".$cid." updated Sucessfully ";
        }

        $category = $this->ns_categories_model->get_category($cid);

       // $categories = $this->ns_categories_model->all_categories();
		$categories = $this->ns_categories_model->to_select();
	/*	echo "<pre>";
		print_r($categories);
		echo "<pre>";
		exit();*/
        
        $this->data['categories'] = $categories;
        $this->data['category'] = $category;
		$this->data['title'] = "Update NS Category";
		$this->data['view'] = 'admin/nscategories/update_nscategory_view';
		$this->template->adminTemplate($this->data);
	}

	public function activateCategory($cid){
		//change the status to "a"
	}

	public function deactivateCategory($cid){
		// change status to "d"
	}

	public function trash($cid) {
		// change status to "t"
	}

	public function deleteCategory($cid){
		// use delete query
	}


}
?>