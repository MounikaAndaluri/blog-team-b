<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  Ion Auth Model
*
* Version: 2.5.2
*
* Author:  Ben Edmunds
* 		   ben.edmunds@gmail.com
*	  	   @benedmunds
*
* Added Awesomeness: Phil Sturgeon
*
* Location: http://github.com/benedmunds/CodeIgniter-Ion-Auth
*
* Created:  10.01.2009
*
* Last Change: 3.22.13
*
* Changelog:
* * 3-22-13 - Additional entropy added - 52aa456eef8b60ad6754b31fbdcc77bb
*
* Description:  Modified auth system based on redux_auth with extensive customization.  This is basically what Redux Auth 2 should be.
* Original Author name has been kept but that does not mean that the method has not been modified.
*
* Requirements: PHP5 or above
*
*/

class posts_model extends CI_Model
{
	
	public function __construct()
	{
		parent::__construct();
	}

	public function get_posts($inputs =null ){
		$posts = null;


			$page = isset($inputs['page']) ? $inputs['page'] : 0;
			$per_page = isset($inputs['per_page']) ? $inputs['per_page'] : 10;
$query = " SELECT * FROM posts ".
			" INNER JOIN categories ".
			" ON posts.category_id = categories.category_id ".
			" INNER JOIN users ".
			" ON users.id = posts.user_id ".
			
			 " WHERE post_status != 't' ".
			" ORDER BY post_updated DESC ".
			" LIMIT $page, $per_page "; 

			

		$posts = $this->db->query($query)->result_array();
		
		return $posts;

	}
public function getOperatorPosts($inputs =null){
		$posts = null;
		$page = isset($inputs['page']) ? $inputs['page'] : 0;
		$per_page = isset($inputs['per_page']) ? $inputs['per_page'] : 10;
		$uid = $inputs['user_id'];
		$query = " SELECT * FROM posts ".
				" INNER JOIN categories ".
				" ON posts.category_id = categories.category_id ".
				" INNER JOIN users ".
				" ON users.id = posts.user_id ".
				
				" WHERE post_status != 't' ".
				" AND user_id = '$uid' ".
				" ORDER BY post_updated DESC ".
				" LIMIT $page, $per_page ";
		
		$posts = $this->db->query($query)->result_array();
		return $posts;
	}
	

	

	public function getPost($pid=null){
		if($pid != null) {
			$post = null;
			$query = null;

				$query = " SELECT * FROM posts ".
				" INNER JOIN categories ".
				" ON posts.category_id = categories.category_id ".
				" WHERE post_id = '$pid' ";

				return $this->db->query($query)->row_array();
		}else{
			return false;
		}
	}

	public function activate_post($inputs = null, $pid = null){
			$query = null;

			if ($pid != null && is_array($inputs)) {
			$this->db->where('post_id', $pid);
			$this->db->update('posts', $inputs);
			}
			return true;

}

	public function deactivate_post($inputs = null, $pid = null){
		$query = null;

		if ($pid != null && is_array($inputs)) {
		$this->db->where('post_id', $pid);
		$this->db->update('posts', $inputs);
		}


		return true;

	}

	public function get_trash_posts($inputs=null){

		$bloodbank = null;
		$query = null;
		$page = isset($inputs['page']) ? $inputs['page'] : 0;
		$per_page = isset($inputs['per_page']) ? $inputs['per_page'] : 10;
		$query = "SELECT * FROM posts ".
		" WHERE posts.post_status = 't' ".
		" LIMIT $page, $per_page ";
		
		$posts = $this->db->query($query)->result_array();
		return $posts;	
	}
	public function get_deactivate_posts($inputs=null){

		$bloodbank = null;
		$query = null;
		$page = isset($inputs['page']) ? $inputs['page'] : 0;
		$per_page = isset($inputs['per_page']) ? $inputs['per_page'] : 10;
		$query = "SELECT * FROM posts ".
		" WHERE posts.post_status = 'd' ".
		" LIMIT $page, $per_page ";
		
		$posts = $this->db->query($query)->result_array();
		return $posts;	
	}


	public function delete_post($pid=null){
		$query = null;
		//$query = " SELECT FROM posts ".
		//		" WHERE post_id = '$pid' ";
		$this->db->delete('posts', array('post_id' => $pid)); 
		return true;
	}


	

}
