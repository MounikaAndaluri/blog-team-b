<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
    <section class="content-header">
           <h1>
             <?php echo lang('create_user_heading');?>
             <small><div id="infoMessage"><?php echo $message;?></div></small>
           </h1>
           <ol class="breadcrumb">
             <li><p></p></li>
            
           </ol>
         </section>

 <!-- Main content -->
         <section class="content">



         <div class="row">
           <div class="col-md-6 col-md-offset-3">
              <!-- Default box -->
           <div class="box">
             <div class="box-header with-border">
               <h3 class="box-title"> <?php echo lang('create_user_subheading'); ?></h3>
               <div class="box-tools pull-right">
                   
                    
               <!--   <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                 <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button> -->
               </div>
             </div>
             <div class="box-body">


<?php echo form_open("admin/create_user", array('role' => 'form'));?>

      <p>
            <?php echo lang('create_user_fname_label', 'first_name');?> <br />
            <?php echo form_input($first_name);?>
      </p>

      <p>
            <?php echo lang('create_user_lname_label', 'last_name');?> <br />
            <?php echo form_input($last_name);?>
      </p>
      
      <?php
      if($identity_column!=='email') {
          echo '<p>';
          echo lang('create_user_identity_label', 'identity');
          echo '<br />';
          echo form_error('identity');
          echo form_input($identity);
          echo '</p>';
      }
      ?>

      <p>
            <?php echo lang('create_user_company_label', 'company');?> <br />
            <?php echo form_input($company);?>
      </p>

      <p>
            <?php echo lang('create_user_email_label', 'email');?> <br />
            <?php echo form_input($email);?>
      </p>

      <p>
            <?php echo lang('create_user_phone_label', 'phone');?> <br />
            <?php echo form_input($phone);?>
      </p>

      <p>
            <?php echo lang('create_user_password_label', 'password');?> <br />
            <?php echo form_input($password);?>
      </p>

      <p>
            <?php echo lang('create_user_password_confirm_label', 'password_confirm');?> <br />
            <?php echo form_input($password_confirm);?>
      </p>


      <p><?php echo form_submit('submit', lang('create_user_submit_btn'), array("class" => "btn btn-info pull-right"));?></p>

<?php echo form_close();?>

             </div><!-- /.box-body -->
              
          </div><!-- /.box -->

           </div>    
         </div>
           







      </section><!-- /.content -->