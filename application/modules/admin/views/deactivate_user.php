<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
 		<section class="content-header">
           <h1>
             <?php echo lang('deactivate_heading');?>
             <small><div id="infoMessage"><?php //echo $message;?></div></small>
           </h1>
           <ol class="breadcrumb">
             <li><p></p></li>
            
           </ol>
         </section>

 <!-- Main content -->
         <section class="content">

           <!-- Default box -->
           <div class="box">
             <div class="box-header with-border">
               <h3 class="box-title"><?php echo sprintf(lang('deactivate_subheading'), $user->username);?></h3>
               <div class="box-tools pull-right">
                   
                    
               <!--   <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                 <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button> -->
               </div>
             </div>
             <div class="box-body">

<?php echo form_open("admin/deactivate/".$user->id);?>

  <p>
  	<?php echo lang('deactivate_confirm_y_label', 'confirm');?>
    <input type="radio" name="confirm" value="yes" checked="checked" />
    <?php echo lang('deactivate_confirm_n_label', 'confirm');?>
    <input type="radio" name="confirm" value="no" />
  </p>

  <?php echo form_hidden($csrf); ?>
  <?php echo form_hidden(array('id'=>$user->id)); ?>

  <p><?php echo form_submit('submit', lang('deactivate_submit_btn'));?></p>

<?php echo form_close();?>

			</div><!-- /.box-body -->
        
       </div><!-- /.box -->

     </section><!-- /.content -->