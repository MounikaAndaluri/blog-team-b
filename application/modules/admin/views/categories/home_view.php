<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
 <!-- Content Header (Page header) -->
         <section class="content-header">
           <h1>
             All Categories 
             <small><div id="infoMessage"><?php echo $message;?></div></small>
           </h1> 
           <ol class="breadcrumb">
           
             <li><a href="<?php echo base_url()."admin/categories/add_category"; ?>">Add Category </a> </li>
              <li><a href="<?php echo base_url()."admin/categories/trashCategories"; ?>">All Trash Categories</a> </li>
            
           </ol>
         </section>

         <!-- Main content -->
         <section class="content">

           <!-- Default box -->
           <div class="box">
             <div class="box-header with-border">
                <?php echo form_open("admin/categories/search", array('role' => 'form', 'method' => 'get'));?>
                  <?php $cname = isset($cname) ? $cname : ""; ?>
                  <input type="text" name="cname" value="<?php echo $cname; ?>" placeholder="Category Name" />
                  <input type="submit" name="csubmit" value="Search" />
                <?php echo form_close();?>
                   <div class="box-tools pull-right">
                        <?php echo $this->pagination->create_links(); ?>
                   <!--   <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                     <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button> -->
                   </div>
             </div>
             <div class="box-body">
                
                 <table class="table">
                  <tr>
                    <th>Category id</th> 
                    <th>Category Name</th> 
                    <th>status</th>
                    <th>Update</th>

                  </tr>
                  <?php foreach ($categories as $cat) { ?>
                    <tr>
                        <td><?php echo $cat['category_id']; ?></td>    
                        <td><?php echo $cat['category_name']; ?></td>
                        <td>
                              <?php 
                                if ($cat['category_status'] == 'a') {
                                    echo 'active | <a href="'.base_url().'admin/categories/deactivateCategory/'.$cat['category_id'].'">deactive</a> | <a href="'.base_url().'admin/categories/trashCategory/'.$cat['category_id'].'">trash</a>';
                                }elseif ($cat['category_status'] == 'd') {
                                    echo '<a href="'.base_url().'admin/categories/activateCategory/'.$cat['category_id'].'">active</a> | deactive | <a href="">trash</a>';
                                } elseif ($cat['category_status'] == 't') {
                                    echo '<a href="'.base_url().'admin/categories/activateCategory/'.$cat['category_id'].'">active</a> | deactive |trash';
                                }
                             ?>
                        </td>
                        <td><a href="<?php echo base_url()."admin/categories/edit_category/".$cat['category_id']; ?>">update</a></td>   
                    </tr>
                  <?php } ?>
                  
                </table>
             
             </div><!-- /.box-body -->
             
           </div><!-- /.box -->

         </section><!-- /.content -->