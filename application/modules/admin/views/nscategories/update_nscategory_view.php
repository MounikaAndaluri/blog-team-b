
    <section class="content-header">
           <h1>
             <?php echo $title; ?>
             <small><div id="infoMessage"><?php echo $message;?></div></small>
           </h1>
           <ol class="breadcrumb">
             <li><p></p></li>
            
           </ol>
         </section>

 <!-- Main content -->
         <section class="content">



         <div class="row">
           <div class="col-md-6 col-md-offset-3">
              <!-- Default box -->
           <div class="box">
             <div class="box-header with-border">
               <h3 class="box-title">Please enter category details</h3>
               <div class="box-tools pull-right">
                   
                    
               <!--   <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                 <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button> -->
               </div>
             </div>
             <div class="box-body">
 <?php echo validation_errors(); ?>

<?php echo form_open("admin/NsCategories/updateCategory/".$category['ns_category_id'], array('role' => 'form', 'enctype' => 'multipart/form-data'));?>
      <div class="form-group">
          <label for="Category Name">Parent category</label>
          <select class="form-control" name="parent_cat_id" id="parent_cat_id">
              <option value=""> Select Parent</option>
              <?php foreach ($categories as $cat_key => $cat_val) { ?>
                    <?php
                        $cat_sel = "";
                        if ($cat_key == $category['ns_category_parent']) {
                          $cat_sel = ' selected="selected" ';
                        }
                    ?>
                    <option value="<?php echo $cat_key; ?>" <?php echo $cat_sel; ?>> <?php echo $cat_val; ?></option>
              <?php } ?>
          </select>
      </div>

      <div class="form-group">
          <label for="Category Name"> Category Name</label>
          <input type="text" name="categoryname" id="categoryname" class="form-control" value="<?php echo $category['ns_category_name']; ?>" />
      </div>

      <div class="form-group">
            <label for="Category Image"> Category Image</label>
            <img src="<?php echo base_url()."images/categories/".$category['ns_category_image']; ?>" width="100" />

            <input type="hidden" name="ocategoryimage" value="<?php echo $category['ns_category_image']; ?>" >
            <input type="file" name="categoryimage" id="categoryimage" class="form-control" />
      </div>

      <div class="form-group">
          <label for="Category Icon"> Category Icon</label>
          <input type="text" name="categoryicon" id="categoryicon" class="form-control" value="<?php echo $category['ns_category_icon']; ?>" />
      </div>

      <div class="form-group">
          <input type="submit" name="submit" id="submit" value="Submit" class="" /> 
      </div>


<?php echo form_close();?>

             </div><!-- /.box-body -->
              
          </div><!-- /.box -->

           </div>    
         </div>
           







      </section><!-- /.content -->