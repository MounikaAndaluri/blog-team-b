 <!-- Content Header (Page header) -->
         <section class="content-header">
           <h1>
             Dashboard
             <?php $message = isset($message) ? $message : ""; ?>
             <small><div id="infoMessage"><?php echo $message;?></div></small>
           </h1>
           <ol class="breadcrumb">
             <li><p><?php echo anchor('admin/create_user', lang('index_create_user_link'))?> </p></li>
            
           </ol>
         </section>

         <!-- Main content -->
         <section class="content">

           <!-- Default box -->
           <div class="box">
             <div class="box-header with-border">
               <h3 class="box-title">Title</h3>
               <div class="box-tools pull-right">
                   <!--  <ul class="pagination pagination-sm no-margin pull-right">
                      <li><a href="#">«</a></li>
                      <li><a href="#">1</a></li>
                      <li><a href="#">2</a></li>
                      <li><a href="#">3</a></li>
                      <li><a href="#">»</a></li>
                    </ul> -->
               <!-- <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                 <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button> -->
               </div>
             </div>
             <div class="box-body">
                
                 <table class="table">
                  <tr>
                    <th>Category id</th> 
                    <th>Category Name</th> 
                    <th>Parent ID</th>
                    <th>Status</th>
                    <th>Update</th>

                  </tr>
                  <?php foreach ($categories as $cat) { ?>
                    <tr>
                        <td><?php echo $cat['ns_category_id']; ?></td>      
                        <td><?php echo $cat['ns_category_name']; ?></td>      
                        <td><?php echo $cat['ns_category_parent']; ?></td>
                        <td>activate | deactivate | trash </td>
                        <td><a href="<?php echo base_url()."admin/nscategories/UpdateCategory/".$cat['ns_category_id']; ?>">Update</a></td>
                    </tr>
  
                  <?php } ?>
                  
                </table>
             
             </div><!-- /.box-body -->
             
           </div><!-- /.box -->

         </section><!-- /.content -->