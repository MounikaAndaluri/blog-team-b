<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
    <section class="content-header">
           <h1>
             <?php echo $title; ?>
             <small><div id="infoMessage"><?php echo $message;?></div></small>
           </h1>
           <ol class="breadcrumb">
             <li><a href="<?php echo base_url()."admin/posts/posts"; ?>">All Posts</a> </li>
             <li><a href="<?php echo base_url()."admin/posts/trashPosts"; ?>">All Trash Posts </a> </li>
            
           </ol>
         </section>

 <!-- Main content -->
         <section class="content">



         <div class="row">
           <div class="col-md-12">
              <!-- Default box -->
           <div class="box">
             <div class="box-header with-border">
               <h3 class="box-title">Please enter category details</h3>
               <div class="box-tools pull-right">
                   
                    
               <!--   <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                 <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button> -->
               </div>
             </div>
             <div class="box-body">
 <?php echo validation_errors(); ?>

<?php echo form_open("admin/posts/add_post", array('role' => 'form', 'enctype' => 'multipart/form-data')); ?>
<div class="col-md-6">
    <div class="form-group">
        <label for="categories">categories</label>
        <input type="text" name="categories" id="categories" class="form-control" value="" />
        <input type="hidden" name="categoryid" id="categoryid" value="" />
    </div>

      <div class="form-group">
        <label for="Category Name"> Post Name</label>
        <input type="text" name="postname" id="postname" class="form-control" />
      </div>

    <div class="form-group">
      <label for="post Slug"> post slug</label>
      <input type="text" name="postslug" id="postslug" class="form-control" value="" />
    </div>

    <div class="form-group">
      <label for="post meta title"> post meta title</label>
      <input type="text" name="postmetatitle" id="postmetatitle" class="form-control" value="" />
    </div>
    <div class="form-group">
      <label for="post meta description"> post meta description</label>
      <input type="text" name="postmetadesc" id="postmetadesc" class="form-control" value="" />
    </div>

    <div class="form-group">
      <label for="post meta keyword"> post meta keywords</label>
      <input type="text" name="postmetakeyword" id="postmetakeyword" class="form-control" value="" />
    </div>

      <div class="form-group">
        <label for="Category Name"> Post Image</label>
      
          <input type="file" name="postimage" id="postimage" class="form-control">

      </div>

      <div class="form-group">
      <label for="post meta keyword"> post image alt</label>
      <input type="text" name="postimagealt" id="postimagealt" class="form-control" value="" />
    </div>
    
      
      <div class="form-group">
          <label for="Category Name"> Post Video Link(i.e youtube)</label>
          <input type="text" name="postvideo" id="postvideo" class="form-control">
      </div>
      

      
</div>


<div class="col-md-6 ">

 <div class="form-group">
        <label for="Category Name"> Post Desc</label>
      
          <textarea name="postdesc" id="postdesc" class="form-control html5editor"></textarea>  
      </div>


     
      
      

      <input type="submit" name="submit" id="submit" value="Submit" class="form-control btn btn-primary" /> 
      


</div>

<?php echo form_close();?>

             </div><!-- /.box-body -->
              
          </div><!-- /.box -->

           </div>    
         </div>
           







      </section><!-- /.content -->