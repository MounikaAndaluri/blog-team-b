 <?php defined('BASEPATH') OR exit('No direct script access allowed');?>
 <!-- Content Header (Page header) -->

         <section class="content-header">
            <h1>
              All Posts 
             <small><div id="infoMessage"><?php echo $message;?></div></small>
           </h1> 
           <ol class="breadcrumb">
             <li><a href="<?php echo base_url()."admin/posts/add_post"; ?>">Add Post </a> </li>
              <li><a href="<?php echo base_url()."admin/posts/trashPosts"; ?>">All Trash Posts</a> </li>
            
           </ol>
         </section>

         <!-- Main content -->
         <section class="content">

           <!-- Default box -->
           <div class="box">
             <div class="box-header with-border">
                <?php echo form_open("admin/posts/search", array('role' => 'form', 'method' => 'get'));?>
                  <?php $pname = isset($pname) ? $pname : ""; ?>
                  <input type="text" name="pname" value="<?php echo $pname; ?>" placeholder="Post Name"/>
                  <input type="submit" name="psubmit" value="Search" />
               
                <?php echo form_close();?>
               <div class="box-tools pull-right">
                     
               <?php echo $this->pagination->create_links(); ?>
                    
               <!--   <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                 <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button> -->
               </div>
             </div>
             <div class="box-body">
                
                 <table class="table">
                  <tr>
                    <th>Post id</th>
                    <th>User Name</th> 
                    <th>Post Name</th> 
                   <!--  <th>Post Description</th> -->
                    <th>Post Image</th>
                    <th>Post Video</th>
                    <th>Status</th>
                    <th>Update</th>


                   

                  </tr>
                  <?php foreach ($posts as $pst) { ?>
                    <tr>
                        <td><?php echo $pst['post_id']; ?></td>
                         <td><a href="<?php echo base_url()."admin/posts/operatorPosts/".$pst['user_id']; ?>"><?php echo $pst['first_name']." ".$pst['last_name']; ?></a></td>      
                        <td><?php echo $pst['post_title']; ?></td> 
                        <!-- <td><?php //echo $pst['post_description']; ?></td> -->
                        <td><img src="<?php echo base_url()."images/posts/".$pst['post_image']; ?>" alt="" width="50" height="50" /></td>
                        <td><?php echo $pst['post_video']; ?></td>  

                        <td>
                              <?php 
                                if ($pst['post_status'] == 'a') {
                                    echo 'active | <a href="'.base_url().'admin/posts/deactivatePost/'.$pst['post_id'].'">deactive</a> | <a href="'.base_url().'admin/posts/trashPost/'.$pst['post_id'].'">trash</a>';
                                }elseif ($pst['post_status'] == 'd') {
                                  echo '<a href="'.base_url().'admin/posts/activatePost/'.$pst['post_id'].'">active</a> | deactive | <a href="">trash</a>';
                                } elseif ($pst['post_status'] == 't') {
                                            echo '<a href="'.base_url().'admin/posts/activatePost/'.$pst['post_id'].'">active</a> | deactive |trash';
                                }
                             ?>
       
                        </td>      
                        <td><a href="<?php echo base_url()."admin/posts/edit_Post/".$pst['post_id']; ?>">Update</a></td>



                        <!-- <td><a href="<?php //echo base_url()."admin/posts/delete_post/".$pst['post_id']; ?>">Delete</a></td> -->

                    </tr>
  
                  <?php } ?>
                  
                </table>
             
             </div><!-- /.box-body -->
             
           </div><!-- /.box -->

         </section><!-- /.content -->