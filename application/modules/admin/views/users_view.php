 <!-- Content Header (Page header) -->
         <section class="content-header">
           <h1>
             All Users
             <small><div id="infoMessage"><?php echo $message;?></div></small>
           </h1>
           <ol class="breadcrumb">
            <!--  <li><p><?php //echo anchor('admin/create_user', lang('index_create_user_link'))?> | <?php //echo anchor('admin/create_group', lang('index_create_group_link'))?></p></li> -->
            
           </ol>
         </section>

         <!-- Main content -->
         <section class="content">

           <!-- Default box -->
           <div class="box">
             <div class="box-header with-border">
               <h3 class="box-title">Title</h3>
               <div class="box-tools pull-right">
                    <ul class="pagination pagination-sm no-margin pull-right">
                      <li><a href="#">«</a></li>
                      <li><a href="#">1</a></li>
                      <li><a href="#">2</a></li>
                      <li><a href="#">3</a></li>
                      <li><a href="#">»</a></li>
                    </ul>
                    
               <!--   <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                 <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button> -->
               </div>
             </div>
             <div class="box-body">
                
                 <table class="table">
                  <tr>
                    <th><?php echo lang('index_fname_th');?></th>
                    <th><?php echo lang('index_lname_th');?></th>
                    <th><?php echo lang('index_email_th');?></th>
                    <th><?php echo lang('index_groups_th');?></th>
                    <th><?php echo lang('index_status_th');?></th>
                    <th><?php echo lang('index_action_th');?></th>
                  </tr>
                  <?php foreach ($users as $user):?>
                    <tr>
                            <td><?php echo htmlspecialchars($user->first_name,ENT_QUOTES,'UTF-8');?></td>
                            <td><?php echo htmlspecialchars($user->last_name,ENT_QUOTES,'UTF-8');?></td>
                            <td><?php echo htmlspecialchars($user->email,ENT_QUOTES,'UTF-8');?></td>
                      <td>
                        <?php foreach ($user->groups as $group):?>
                          <?php echo htmlspecialchars($group->name,ENT_QUOTES,'UTF-8'); ?>
                          <?php //echo anchor("admin/edit_group/".$group->id, htmlspecialchars($group->name,ENT_QUOTES,'UTF-8')) ;?>
                          <br />
                        <?php endforeach?>
                      </td>
                      <td><?php echo ($user->active) ? anchor("admin/deactivate/".$user->id, lang('index_active_link')) : anchor("admin/activate/". $user->id, lang('index_inactive_link'));?></td>
                      <td><?php echo anchor("admin/edit_user/".$user->id, 'Update') ;?></td>
                    </tr>
                  <?php endforeach;?>
                </table>
             
             </div><!-- /.box-body -->
             
           </div><!-- /.box -->

         </section><!-- /.content -->