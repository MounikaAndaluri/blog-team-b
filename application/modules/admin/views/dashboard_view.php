<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
 <!-- Content Header (Page header) -->
         <section class="content-header">
           <h1>
             Admin    Dashboard
             <small><div id="infoMessage"><?php echo $message;?></div></small>
           </h1>
           <ol class="breadcrumb">
           <!--   <li><p><?php //echo anchor('admin/create_user', lang('index_create_user_link'))?> | <?php //echo anchor('admin/create_group', lang('index_create_group_link'))?></p></li> -->
            
           </ol>
         </section>

         <!-- Main content -->
         <section class="content ">

           <!-- Default box -->
           <div class="box">
             <div class="box-header with-border">
               <h3 class="box-title"><?php echo $title; ?> </h3>
               <div class="box-tools pull-right">
                    <!-- <ul class="pagination pagination-sm no-margin pull-right">
                      <li><a href="#">«</a></li>
                      <li><a href="#">1</a></li>
                      <li><a href="#">2</a></li>
                      <li><a href="#">3</a></li>
                      <li><a href="#">»</a></li>
                    </ul> -->
                    
               <!--   <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                 <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button> -->
               </div>
             </div>
             <div class="box-body">

              <!-- Main content -->
        <section class="content matter">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
                  <h3>
                    <?php 
                      $total_row = $this->db->select('post_id')->from('posts')->where('post_status !=', 't')->count_all_results();
                      echo $total_row;
                  ?>
                  </h3>
                  <p>Blog Posts</p>
                </div>
                <div class="icon">
                  <i class="fa fa-h-square" aria-hidden="true"></i>
                </div>
                <a href="<?php echo base_url()."admin/posts"; ?>" class="small-box-footer">View All <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->



         
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <h3>
                      <?php 
                        $total_row = $this->db->select('category_id')->from('categories')->where('category_status !=', 't')->count_all_results();
                        echo $total_row;
                      ?>
                  </h3>
                  <p>Blog Categories</p>
                </div>
                <div class="icon">
               <i class="fa fa-h-square" aria-hidden="true"></i>
                </div>
                <a href="<?php echo base_url()."admin/categories"; ?>" class="small-box-footer">View All <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->

       




          </div><!-- /.row -->
          <!-- Main row -->
          

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->


                    <!-- TABLE: LATEST ORDERS -->
              <section class="content">
                <div class="row">
                  <div class="col-md-12">
                    
             <h3 class="box-title" >
             Latest Posts
             </h3>        
              <div class="box box-info ">
                <div class="box-header with-border">
                  
                  <div >
                   <?php echo form_open("admin/posts/search", array('role' => 'form', 'method' => 'get'));?>
                  <?php $pname = isset($pname) ? $pname : ""; ?>
                  <input type="text" name="pname" value="<?php echo $pname; ?>" placeholder="Post Name" required/>
                  <input type="submit" name="psubmit" value="Search" />
               
                <?php echo form_close();?>
                </div>
                  <div class="box-tools pull-right" >
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <div class="table-responsive">
                    <table class="table no-margin">
                      <thead>
                        <tr>
                          <th>Post ID</th>
                          <th>User Name</th>
                           <th>Category ID</th>
                          <th>Post Name</th>
                          <th>Status</th>
                         
                        </tr>
                      </thead>
                      <tbody>
                      <?php foreach($posts as $p ){?>
                        <tr>
                          <td><a href=""><?php echo $p['post_id']?></a></td>
                           <td><a href="<?php echo base_url()."admin/posts/operatorPosts/".$p['user_id']; ?>"><?php echo $p['first_name']." ".$p['last_name']; ?></a></td>
                          <td><?php echo $p['category_id']?></td>
                          <td><?php echo $p['post_title']?></td>
                          <td>  <?php 
                                if ($p['post_status'] == 'a') {
                                    echo 'active | <a href="'.base_url().'admin/posts/deactivatePost/'.$p['post_id'].'">deactive</a> | <a href="'.base_url().'admin/posts/trashPost/'.$p['post_id'].'">trash</a>';
                                }elseif ($p['post_status'] == 'd') {
                                  echo '<a href="'.base_url().'admin/posts/activatePost/'.$p['post_id'].'">active</a> | deactive | <a href="">trash</a>';
                                } elseif ($p['post_status'] == 't') {
                                            echo '<a href="'.base_url().'admin/posts/activatePost/'.$p['post_id'].'">active</a> | deactive |trash';
                                }
                             ?></td>
                          
                        </tr>
                        
                        <?php }?>
                      
                     
                      </tbody>
                    </table>
                  </div><!-- /.table-responsive -->
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
                  <a href="<?php echo base_url()."admin/posts/add_post"?>" class="btn btn-sm btn-info btn-flat pull-left">Place New Post</a>
                  <a href="<?php echo base_url()."admin/posts"?>" class="btn btn-sm btn-default btn-flat pull-right">View All Posts</a>
                </div><!-- /.box-footer -->

                   <?php echo $this->pagination->create_links(); ?>
              </div><!-- /.box -->


 </div>
                </div>
              </section>

      
      
      
      
      
      
      
      
      
      
      
      
      

                
                 
             </div><!-- /.box-body -->
             
           </div><!-- /.box -->

         </section><!-- /.content -->