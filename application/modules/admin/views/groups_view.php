 <!-- Content Header (Page header) -->
         <section class="content-header">
           <h1>
             User Groups
             <small><div id="infoMessage"><?php echo $message;?></div></small>
           </h1>
           <ol class="breadcrumb">
             <!-- <li><p><?php //echo anchor('admin/create_user', lang('index_create_user_link'))?> | <?php //echo anchor('admin/create_group', lang('index_create_group_link'))?></p></li> -->
            
           </ol>
         </section>

         <!-- Main content -->
         <section class="content">

           <!-- Default box -->
           <div class="box">
             <div class="box-header with-border">
               <h3 class="box-title"><?php echo $title; ?></h3>
               <!-- <div class="box-tools pull-right">
                    <ul class="pagination pagination-sm no-margin pull-right">
                      <li><a href="#">«</a></li>
                      <li><a href="#">1</a></li>
                      <li><a href="#">2</a></li>
                      <li><a href="#">3</a></li>
                      <li><a href="#">»</a></li>
                    </ul>
                     -->
               <!--   <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                 <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button> -->
               <!-- </div> -->
             </div>
             <div class="box-body">
                
<table class="table">
	<tr>
		<th>Group id</th>
		<th>Group Name</th>
	</tr>
	<?php foreach ($groups as $group):?>
		<tr>
            <td><?php echo htmlspecialchars($group->id,ENT_QUOTES,'UTF-8');?></td>
            <td><?php echo htmlspecialchars($group->name,ENT_QUOTES,'UTF-8');?></td>
		</tr>
	<?php endforeach;?>
</table>


             </div><!-- /.box-body -->
             
           </div><!-- /.box -->

         </section><!-- /.content -->