<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
    <section class="content-header">
           <h1>
             <?php echo lang('create_group_heading');?>
             <small><div id="infoMessage"><?php echo $message;?></div></small>
           </h1>
           <ol class="breadcrumb">
             <li><p></p></li>
            
           </ol>
         </section>

 <!-- Main content -->
         <section class="content">



         <div class="row">
           <div class="col-md-6 col-md-offset-3">
              <!-- Default box -->
           <div class="box">
             <div class="box-header with-border">
               <h3 class="box-title"> <?php echo lang('create_group_subheading'); ?></h3>
               <div class="box-tools pull-right">
                   
                    
               <!--   <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                 <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button> -->
               </div>
             </div>
             <div class="box-body">


<?php echo form_open("admin/create_group");?>

      <p>
            <?php echo lang('create_group_name_label', 'group_name');?> <br />
            <?php echo form_input($group_name);?>
      </p>

      <p>
            <?php echo lang('create_group_desc_label', 'description');?> <br />
            <?php echo form_input($description);?>
      </p>

      <p><?php echo form_submit('submit', lang('create_group_submit_btn'), array("class" => "btn btn-info pull-right"));?></p>

<?php echo form_close();?>
 			</div><!-- /.box-body -->
              
          </div><!-- /.box -->

           </div>    
         </div>
           







      </section><!-- /.content -->