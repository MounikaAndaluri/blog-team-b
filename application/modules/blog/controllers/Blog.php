<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends MY_Controller {

	function __construct(){
	     parent::__construct();
	     $this->load->model('blog/blogs_model');
	     // we have to load the blog categories  // menu purpose
	}

	public function index()
	{
		$this->load->library('pagination');
		$data = null;
		$posts = null;

		///$state = rawurldecode(utf8_decode($state));
      
        $config = array();
        $config["base_url"] = base_url()."blog/posts/";
        $total_row = $this->db->select('post_id')->where('post_status', '1')->from('posts')->count_all_results();

        $config["total_rows"] = $total_row;
        $config["per_page"] = 10;
        $config['num_links'] = 15;
        $config["uri_segment"] = 4;

        /* This Application Must Be Used With BootStrap 3 *  */
        $config['full_tag_open'] = "<ul class='pagination pagination-flat pagination-separated-np'>";
        $config['full_tag_close'] ="</ul>";

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = "</a></li>";

        $config['next_tag_open'] = '<li>';

        $config['next_tagl_close'] = "</li>";

        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";

        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

		$posts = $this->blogs_model->getPosts($page, $config['per_page']);

		
		$categories = $this->blogs_model->getCategories();
		$rposts = $this->blogs_model->getRecentPosts();
		
		$data['categories']=$categories;
		$data['posts'] = $posts;
		
		$data['rposts'] = $rposts;
		$data['view'] = 'blog/category_posts_view';
    	$this->template->HomeTemplate($data);
	}

	public function about(){
	    $data = null;
	  
	    $data['view'] = 'blog/about_view';
	    $this->template->HomeTemplate($data);
	}

	public function post($slug=null){
		$data = null;
		$post = null;
		$categories=null;

		
		$post = $this->blogs_model->getPostBySlug($slug);
		//$posts = $this->blogs_model->getRecentPosts();
		$posts = $this->blogs_model->getCategoryPosts($post['category_id']);
		$categories=$this->blogs_model->getCategories();
		$data['post'] = $post;
		$data['posts'] = $posts;
		$data['categories']=$categories;
		$data['mtitle'] = $post['post_meta_title'];
		$data['mdesc'] = $post['post_meta_description'];
		$data['mkeywords'] = $post['post_meta_keywords'];
		$data['view'] = 'blog/post_view';
    	$this->template->HomeTemplate($data);
	}

	public function categoryPosts($cid=null){
		
		$this->load->library('pagination');
		$data = null;
		$posts = null;

		///$state = rawurldecode(utf8_decode($state));
      
        $config = array();
        $config["base_url"] = base_url()."blog/category_posts/".$cid;
        $total_row = $this->db->select('post_id')->where('category_id', $cid)->from('posts')->count_all_results();

        $config["total_rows"] = $total_row;
        $config["per_page"] = 10;
        $config['num_links'] = 15;
        $config["uri_segment"] = 4;

        /* This Application Must Be Used With BootStrap 3 *  */
        $config['full_tag_open'] = "<ul class='pagination pagination-flat pagination-separated-np'>";
        $config['full_tag_close'] ="</ul>";

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = "</a></li>";

        $config['next_tag_open'] = '<li>';

        $config['next_tagl_close'] = "</li>";

        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";

        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;



		$posts = $this->blogs_model->getCategoryPosts($cid, $page, $config['per_page']);

		
		$categories = $this->blogs_model->getCategories();
		$rposts = $this->blogs_model->getRecentPosts();
		 
		$data['categories']=$categories;
		$data['posts'] = $posts;
		
		$data['rposts'] = $rposts;
		$data['view'] = 'blog/category_posts_view';
    	$this->template->HomeTemplate($data);
	}

	public function posts(){
			
		$this->load->library('pagination');
		$data = null;
		$posts = null;

		///$state = rawurldecode(utf8_decode($state));
      
        $config = array();
        $config["base_url"] = base_url()."blog/posts/";
        $total_row = $this->db->select('post_id')->where('post_status', '1')->from('posts')->count_all_results();

        $config["total_rows"] = $total_row;
        $config["per_page"] = 10;
        $config['num_links'] = 15;
        $config["uri_segment"] = 4;

        /* This Application Must Be Used With BootStrap 3 *  */
        $config['full_tag_open'] = "<ul class='pagination pagination-flat pagination-separated-np'>";
        $config['full_tag_close'] ="</ul>";

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = "</a></li>";

        $config['next_tag_open'] = '<li>';

        $config['next_tagl_close'] = "</li>";

        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";

        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

		$posts = $this->blogs_model->getPosts($page, $config['per_page']);

		
		$categories = $this->blogs_model->getCategories();
		$rposts = $this->blogs_model->getRecentPosts();
		
		$data['categories']=$categories;
		$data['posts'] = $posts;
		
		$data['rposts'] = $rposts;
		$data['view'] = 'blog/category_posts_view';
    	$this->template->HomeTemplate($data);
	}
}
