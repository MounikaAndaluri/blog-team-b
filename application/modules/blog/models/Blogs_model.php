<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  Ion Auth Model
*
* Version: 2.5.2
*
* Author:  Ben Edmunds
* 		   ben.edmunds@gmail.com
*	  	   @benedmunds
*
* Added Awesomeness: Phil Sturgeon
*
* Location: http://github.com/benedmunds/CodeIgniter-Ion-Auth
*
* Created:  10.01.2009
*
* Last Change: 3.22.13
*
* Changelog:
* * 3-22-13 - Additional entropy added - 52aa456eef8b60ad6754b31fbdcc77bb
*
* Description:  Modified auth system based on redux_auth with extensive customization.  This is basically what Redux Auth 2 should be.
* Original Author name has been kept but that does not mean that the method has not been modified.
*
* Requirements: PHP5 or above
*
*/

class blogs_model extends CI_Model
{
	
	public function __construct()
	{
		parent::__construct();
	}

	public function getPosts($page="0", $perpage="10"){
	$posts = null;

		$query = " SELECT * FROM posts ".
				" INNER JOIN categories ".
				" ON posts.category_id = categories.category_id ".
				//" WHERE posts.category_id = '$cid' ".
				" ORDER BY posts.post_updated DESC ".
				" LIMIT $page, $perpage " ;

		$posts = $this->db->query($query)->result_array();
		return $posts;

	}

	public function getRecentPosts($page="0", $perpage="10"){
		$posts = null;

		$query = " SELECT * FROM posts ".
				" INNER JOIN categories ".
				" ON posts.category_id = categories.category_id ".
				" ORDER BY posts.post_updated DESC ".
				" LIMIT $page, $perpage " ;

		$posts = $this->db->query($query)->result_array();
		return $posts;
	}

	public function getPostById($pid=null){
		if($pid != null) {
			$post = null;
			$query = null;

				$query = " SELECT * FROM posts ".
				" INNER JOIN categories ".
				" ON posts.category_id = categories.category_id ".
				" WHERE post_id = '$pid' ";

				return $this->db->query($query)->row_array();
		}else{
			return false;
		}
	}
	public function getPostBySlug($slug=null){
		if($slug != null) {
			$post = null;
			$query = null;

				$query = " SELECT * FROM posts ".
				" INNER JOIN categories ".
				" ON posts.category_id = categories.category_id ".
				" WHERE post_slug = '$slug' ";

				return $this->db->query($query)->row_array();
		}else{
			return false;
		}
	}

	public function getCategories(){
		$query = null;

		$query =" select * from categories";
		return $this->db->query($query)->result_array();
	}

	public function getCategoryPosts($cid=null, $page="0", $perpage="10"){
		$posts = null;
	
		$query = " SELECT * FROM posts ".
				" INNER JOIN categories ".
				" ON posts.category_id = categories.category_id ".
				" WHERE posts.category_id = '$cid' ".
				" ORDER BY posts.post_updated DESC".
				" LIMIT $page, $perpage " ;

		$posts = $this->db->query($query)->result_array();
		return $posts;
	}

public function getCategoryPostsCount($cid=null, $page="0", $perpage="10"){
		$posts = null;

		$query = " SELECT * FROM posts ".
				" INNER JOIN categories ".
				" ON posts.category_id = categories.category_id ".
				" WHERE posts.category_id = '$cid' ".
				" ORDER BY posts.post_updated ".
				" LIMIT $page, $perpage ";
				

		$posts = $this->db->query($query)->result_array();
		return $posts;
	}



}
