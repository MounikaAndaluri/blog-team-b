<?php defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . 'modules/api/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */

class Users extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        /* Load form helper */ 
        $this->load->helper(array('form'));    
        $this->load->library('form_validation');

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['use']['limit'] = 500; // 500 requests per hour per user/key
    //    $this->methods['add_user']['limit'] = 100; // 100 requests per hour per user/key
    //    $this->methods['edit_user']['limit'] = 50; // 50 requests per hour per user/key

    }

    function login_post(){
        $user_email = $this->input->post('user_email');
        $user_pwd = $this->input->post('user_pwd');
        $device_type = $this->input->post('device_type');
        $device_token = $this->input->post('device_token');

        $tables = $this->config->item('tables','ion_auth');
        
        $config = array(
                array(
                        'field' => 'user_email',
                        'label' => 'user_email',
                        'rules' => 'required|valid_email',
                ),
                array(
                        'field' => 'user_pwd',
                        'label' => 'user_pwd',
                        'rules' => 'required',
                ),
                array(
                        'field' => 'device_type',
                        'label' => 'device_type',
                        'rules' => 'required|trim|strtoupper|in_list[I,A]',
                        'errors' => array(
                            'in_list' => ' %s field must be "A" (Android) or "I" (IOS).'
                        )
                ),
                 array(
                        'field' => 'device_token',
                        'label' => 'device_token',
                        'rules' => 'required',
                ),
        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() === TRUE)
        {
             $remember = (bool) $this->input->post('remember');
             if($this->ion_auth->login($user_email,$user_pwd, $remember)){
                // LOGIN TRUE
                $query ="SELECT users.id as user_id, users.first_name, users.last_name, users.email, users.photo,".
                " users.phone, users.date_of_birth, users.gender, groups.name as role_name , case groups.name when 'members' then 'U' when 'gym_owner' then 'P' else 'N' end as user_type".
                " FROM users ".
                " INNER JOIN users_groups ".
                " ON users_groups.user_id = users.id".
                " INNER JOIN groups ".
                " ON groups.id = users_groups.group_id ".
                " WHERE users.email = '$user_email' ";
                //" AND groups.name = 'members' ";

                $user = $this->db->query($query)->row();
                if (!empty($user->photo))
				$user->photo = base_url()."images/profiles/".$user->photo;
                
                if ($user) {
                    
                    //if ($user->role_name == 'member'){
                        
                        $user_id = $user->user_id;
                        $device_update_query = "update users set device_type='$device_type', device_token='$device_token' where id='$user_id'";
                        $this->db->query($device_update_query);

                       // $this->logout_get(); // Here session created on server because of ion-auth so we need to clear the session
                        $msg = array("status" => true, "message" => "success", "data" => $user);
                        $this->set_response($msg, REST_Controller::HTTP_OK);
                    /*
                    } else{
                        $msg = $msg = array("status" => false, "message" => "user role is not a member"); // Logged in user not in a 'member' group: login details are correct but role is diffrent 
                        $this->set_response($msg,REST_Controller::HTTP_UNAUTHORIZED);
                    }*/
                }else{
                    $msg = $msg = array("status" => false, "message" => "user not found");
                    $this->set_response($msg,REST_Controller::HTTP_UNAUTHORIZED);
                }

            }else{
                $msg = array("status" => false, "message" => "Incorrect login details");
                //$msg['pdata'] = $this->input->post();
                $this->set_response($msg,REST_Controller::HTTP_UNAUTHORIZED);
            }
        }else {
            // display the create user form
            // set the flash data error message if there is one
            $data_err = strip_tags(validation_errors());
            $data_error = explode("\n", $data_err);
            //unset($data_error[count($data_error) - 1]);
            array_pop($data_error); // removes last element
            $msg = array("status" => false, "message" => "fail", "error"=>$data_error);
            $this->set_response($msg, REST_Controller::HTTP_BAD_REQUEST);
        }   
    }

    function update_user_pic_post(){

        $user_id = $this->input->post('user_id');
        $photo = $this->input->post('photo');

        $imageName = null;

        $config = array( 
                array(
                        'field' => 'user_id',
                        'label' => 'user_id',
                        'rules' => 'required|trim',
                ),
                array(
                        'field' => 'photo',
                        'label' => 'photo',
                        //'rules' => 'valid_base64|trim',
                        'rules' => 'trim',
                        'errors' => array(
                            'valid_base64' => ' %s field must be base64 format - string after ( data:image/png;base64, ) is required.'
                        )
                )
        );


        if(!empty($photo)) { // base64 string
                $data['upload_path'] = APPPATH.'../images/profiles/';
                //echo APPPATH;
                //exit();
                $imageDataEncoded = $this->input->post('photo');
                // $imageDataEncoded = base64_encode(file_get_contents('sample.png')); encode the image to base64
                $imageData = base64_decode($imageDataEncoded);
                $source = imagecreatefromstring($imageData);
                // $angle = 360;
                // $rotate = imagerotate($source, $angle, 0); // if want to rotate the image
                $imageName = md5(time().uniqid()).".jpg";
                $imgpath = $data['upload_path'].$imageName;
                $imageSave = imagejpeg($source,$imgpath,100);
                imagedestroy($source);

                $post_data = array(
                    'photo' => $imageName,
                     );

                $data =  array();

                $data['photo'] = base_url().'images/profiles/'.$imageName;

                if($this->ion_auth->update($user_id, $post_data)){
                    $msg = array("status" => true, "message" => "success", "data"=>$data);
                    $this->set_response($msg, REST_Controller::HTTP_OK);
                }else{
                    $data_error = "unable to upload.";
                    $msg = array("status" => false, "message" => "fail", "error"=>$data_error);
                    $this->set_response($msg, REST_Controller::HTTP_BAD_REQUEST);
                }

            }else{
                $data_error = "unable to upload.";
                $msg = array("status" => false, "message" => "fail", "error"=>$data_error);
                $this->set_response($msg, REST_Controller::HTTP_BAD_REQUEST);
            }
    }


    function update_details_post(){
        $user_id = $this->input->post('user_id');
        $firstname = $this->input->post('firstname');
        $lastname = $this->input->post('lastname');
        $dob = $this->input->post('dob');
        $gender = $this->input->post('gender');
        $pwd = $this->input->post('pwd');
        $cpwd = $this->input->post('cpwd');

        $config = array(
                array(
                        'field' => 'user_id',
                        'label' => 'user_id',
                        'rules' => 'required',
                ),
                array(
                        'field' => 'firstname',
                        'label' => 'firstname',
                        'rules' => 'required|trim',
                ),
                
                array(
                        'field' => 'lastname',
                        'label' => 'lastname',
                        'rules' => 'required|trim',
                ),
                array(
                        'field' => 'dob',
                        'label' => 'dob',
                        'rules' => 'required|trim|valid_date[Y-m-d]',
                        //'rules' => 'required|trim|valid_date[Y-m-d h:i:s]',
                ),
                array(
                        'field' => 'gender',
                        'label' => 'gender',
                        'rules' => 'required|trim|strtolower|in_list[m,f]',
                        'errors' => array(
                            'in_list' => ' %s field must be m or f.'
                        )
                ),
                array(
                        'field' => 'pwd',
                        'label' => 'pwd',
                        'rules' => 'required|trim|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[cpwd]',
                ),
                array(
                        'field' => 'cpwd',
                        'label' => 'cpwd',
                        'rules' => 'required|trim',
                )
            );

            $this->form_validation->set_rules($config);

            if ($this->form_validation->run() === TRUE)
            {
                $post_data = array(
                    'first_name'=>$firstname,
                    'last_name'=>$lastname,
                    'date_of_birth'=>$dob,
                    'gender'=>$gender,
                    'password'=>$pwd,
                     );

                $data = array();


                if($this->ion_auth->update($user_id, $post_data)){
                    $data['first_name'] = $firstname;
                    $data['last_name'] = $lastname;
                    $data['dob'] = $dob;
                    $data['gender'] = $gender;
                    $data['password'] = $pwd;
                    $msg = array("status" => true, "message" => "success", "data"=>$data);
                    $this->set_response($msg, REST_Controller::HTTP_OK);
                }else{
                    $data_error = "unable to upload.";
                    $msg = array("status" => false, "message" => "fail", "error"=>$data_error);
                    $this->set_response($msg, REST_Controller::HTTP_BAD_REQUEST);
                }

            }else {
            // display the create user form
            // set the flash data error message if there is one
            $data_err = strip_tags(validation_errors());
            $data_error = explode("\n", $data_err);
            //unset($data_error[count($data_error) - 1]);
            array_pop($data_error); // removes last element
            $msg = array("status" => false, "message" => "fail", "error"=>$data_error);
            $this->set_response($msg, REST_Controller::HTTP_BAD_REQUEST);
        }
    }



    function register_post(){

        $firstname = $this->input->post('firstname');
        $lastname = $this->input->post('lastname');
        $email = $this->input->post('email');
        $phone = $this->input->post('phone');
        $dob = $this->input->post('dob');
        $gender = $this->input->post('gender');
        $pwd = $this->input->post('pwd');
        $cpwd = $this->input->post('cpwd');
        $devicetype = $this->input->post('device_type');
        $usertype = $this->input->post('usertype');
        $imageName = null;
        $groups = null;

        $tables = $this->config->item('tables','ion_auth');
        $identity_column = $this->config->item('identity','ion_auth');
        $this->data['identity_column'] = $identity_column;

        $config = array(
                array(
                        'field' => 'firstname',
                        'label' => 'firstname',
                        'rules' => 'required|trim',
                ),
                
                array(
                        'field' => 'lastname',
                        'label' => 'lastname',
                        'rules' => 'required|trim',
                ),

                array(
                        'field' => 'email',
                        'label' => 'email',
                        'rules' => 'required|strtolower|trim|valid_email|is_unique[' . $tables['users'] . '.email]',
                ),
                array(
                        'field' => 'phone',
                        'label' => 'phone',
                        'rules' => 'required|regex_match[/^[0-9]{10}$/]|trim|is_unique['.$tables['users'].'.phone]',
                ),
                array(
                        'field' => 'dob',
                        'label' => 'dob',
                        'rules' => 'required|trim|valid_date[Y-m-d]',
                        //'rules' => 'required|trim|valid_date[Y-m-d h:i:s]',
                ),
                array(
                        'field' => 'gender',
                        'label' => 'gender',
                        'rules' => 'required|trim|strtolower|in_list[m,f]',
                        'errors' => array(
                            'in_list' => ' %s field must be m or f.'
                        )
                ),
                array(
                        'field' => 'usertype',
                        'label' => 'usertype',
                        'rules' => 'required|trim|strtolower|in_list[u,p]',
                        'errors' => array(
                            'in_list' => ' %s field must be "u" (user) or "p" (partner or gym owner).'
                        )
                ),
                array(
                        'field' => 'pwd',
                        'label' => 'pwd',
                        'rules' => 'required|trim|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[cpwd]',
                ),
                array(
                        'field' => 'cpwd',
                        'label' => 'cpwd',
                        'rules' => 'required|trim',
                ),
                array(
                        'field' => 'photo',
                        'label' => 'photo',
                        //'rules' => 'valid_base64|trim',
                        'rules' => 'trim',
                        'errors' => array(
                            'valid_base64' => ' %s field must be base64 format - string after ( data:image/png;base64, ) is required.'
                        )
                ),
                array(
                        'field' => 'device_type',
                        'label' => 'device_type',
                        'rules' => 'required|trim|strtoupper|in_list[I,A]',
                        'errors' => array(
                            'in_list' => ' %s field must be "A" (Android) or "I" (IOS).'
                        )
                ),
                 array(
                        'field' => 'device_token',
                        'label' => 'device_token',
                        'rules' => 'required',
                ),
        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() === TRUE)
        {

            if(!empty($this->input->post('photo'))) { // base64 string
                $data['upload_path'] = APPPATH.'../images/profiles/';
                $imageDataEncoded = $this->input->post('photo');
                // $imageDataEncoded = base64_encode(file_get_contents('sample.png')); encode the image to base64
                $imageData = base64_decode($imageDataEncoded);
                $source = imagecreatefromstring($imageData);
                // $angle = 360;
                // $rotate = imagerotate($source, $angle, 0); // if want to rotate the image
                $imageName = md5(time().uniqid()).".jpg";
                $imgpath = $data['upload_path'].$imageName;
                $imageSave = imagejpeg($source,$imgpath,100);
                imagedestroy($source);
            }
            
            $email    = $this->input->post('email');
            $identity = ($identity_column==='email') ? $email : $this->input->post('identity');
            $password = $pwd;

            if ($usertype == 'p') {
                $groups = array('3'); // group id's 3 - gym_owner group
            }elseif ($usertype == 'u') {
                $groups = array('2'); // group id's 2 - Members group
            }else{
                $groups = array('2');
            }
            
           $additional_data = array(
                    'first_name' => $this->input->post('firstname'),
                    'last_name'  => $this->input->post('lastname'),
                    'phone'      => $this->input->post('phone'),
                    'date_of_birth' => $this->input->post('dob'),
                    'gender' => $this->input->post('gender'),
                    'device_type' => $this->input->post('device_type'),
                    'device_token' => $this->input->post('device_token'),
                    'photo' => $imageName,
            );
    
            if ($userid = $this->ion_auth->register($identity, $password, $email, $additional_data,$groups))
            {
                // check to see if we are creating the user
                // redirect them back to the admin page
               //$user['user_id'] = $userid;
                
                $query ="SELECT users.id as user_id, users.first_name, users.last_name, users.email, users.photo,".
                " users.phone, users.date_of_birth, users.gender, groups.name as role_name, case groups.name when 'members' then 'u' when 'gym_owner' then 'p' else 'n' end as user_type".
                " FROM users ".
                " INNER JOIN users_groups ".
                " ON users_groups.user_id = users.id".
                " INNER JOIN groups ".
                " ON groups.id = users_groups.group_id ".
                " WHERE users.id = '$userid' ";
                //" AND groups.name = 'members' ";

                $user = $this->db->query($query)->row();

                // 
                $user->date_of_birth = date('Y-m-d', strtotime($user->date_of_birth));

               // $user->role_name = "members";
                $msg = array("status" => true, "message" => "success", "data" => $user);
                $this->set_response($msg, REST_Controller::HTTP_OK);
            
            }else{
                // set the flash data error message if there is one
                //$this->data['message'] = $this->ion_auth->errors();
                $data_err = strip_tags($this->ion_auth->errors());
                //$data_error = explode("\n", $data_err);
                //unset($data_error[count($data_error) - 1]);
                //array_pop($data_error); // removes last element
                $this->set_response($data_err, REST_Controller::HTTP_BAD_REQUEST);
            }
        }else {
            // display the create user form
            // set the flash data error message if there is one
            $data_err = strip_tags(validation_errors());
            $data_error = explode("\n", $data_err);
            //unset($data_error[count($data_error) - 1]);
            array_pop($data_error); // removes last element
            $msg = array("status" => false, "message" => "fail", "error"=>$data_error);
            $this->set_response($msg, REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    
    function edit_user_post(){
            $email    = strtolower($this->input->post('email'));
            $identity = $email;
            $password = $this->input->post('password');
            $profilepic = "";
            $uid = $this->input->post('uid');

            if (isset($_FILES['profilepic']) && is_array($_FILES['profilepic'])) {
                $config['upload_path'] = './images/profiles/';
                $config['allowed_types'] = '*';
                $config['encrypt_name'] = TRUE;
                $this->load->library('upload', $config);

                if (!$this->upload->do_upload('profilepic')) {
                            $error = array('error' => $this->upload->display_errors());
                            $this->response($error, REST_Controller::HTTP_BAD_REQUEST);
                }   else{
                            $gymlogo = array('upload_data' => $this->upload->data());
                            $profilepic = $gymlogo['upload_data']['file_name'];
                }
            }

            $additional_data = array(
                    'first_name' => $this->input->post('firstname'),
                    'last_name'  => $this->input->post('lastname'),
                    //'company'    => $this->input->post('company'),
                    'phone'      => $this->input->post('phone'),
                    'date_of_birth' => $this->input->post('dateofbirth'),
                    'gender' => $this->input->post('gender'),
                //  'device_type' => $this->input->post('device_type'),
                //   'device_token' => $this->input->post('device_token'),
                    'photo' => $profilepic,
            );

           /* if ($this->ion_auth->register($identity, $password, $email, $additional_data))
            {
                //$user = $this->ion_auth->user()->row();
                $message = array("status" => "ok", "message" => "Updated successfull");
                $this->set_response($message, REST_Controller::HTTP_OK);  
            }
            else
            {
                // Set the response and exit
                    $this->response([
                        'status' => FALSE,
                        'message' => 'Update Failed'
                    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code   
            }*/

    }

    function logout_get() {
            $this->ion_auth->logout();
    }

    function get_user_by_id_post() { // This function accepts get parameters
      
        $id = $this->input->post('user_id');
        if ($id > 0) {

            //RAW QUERY STYLE
                $query = " SELECT users.id as user_id, users.first_name, users.last_name, users.email, users.photo, ".
                " users.phone, users.date_of_birth, users.gender ".
                " FROM users ".
                " INNER JOIN users_groups ".
                " ON users_groups.user_id = users.id".
                " INNER JOIN groups ".
                " ON groups.id = users_groups.group_id ".
                " WHERE users.id = '$id' ".
                " AND groups.name='members' ";
                
                $user = $this->db->query($query)->row();
                
                 $user->date_of_birth = date('Y-m-d', strtotime($user->date_of_birth));

                if ($user) {

                    $this->set_response($user, REST_Controller::HTTP_OK);    
                }else{
                    // Set the response and exit
                    $this->response([
                        'status' => FALSE,
                        'message' => 'No users were found'
                    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code   
                }

        }else{
            // Set the response and exit
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }


    function user_by_email_get($email = null) { // This function accepts get parameters

        if (!empty($email)) {

            //RAW QUERY STYLE
                $query = " SELECT users.id as user_id, users.first_name, users.last_name, users.email, users.photo, ".
                " users.phone, users.phone, users.date_of_birth, users.gender ".
                " FROM users ".
                " INNER JOIN users_groups ".
                " ON users_groups.user_id = users.id".
                " INNER JOIN groups ".
                " ON groups.id = users_groups.group_id ".
                " WHERE users.email = '$email' ".
                " AND groups.name='members' ";
                
                $user = $this->db->query($query)->row();
                
                $user->date_of_birth = date('Y-m-d', strtotime($user->date_of_birth));

                if ($user) {

                    $this->set_response($user, REST_Controller::HTTP_OK);    
                }else{
                    // Set the response and exit
                    $this->response([
                        'status' => FALSE,
                        'message' => 'No users were found'
                    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code   
                }

        }else{
            // Set the response and exit
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }


}
