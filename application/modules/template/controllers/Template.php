<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Template extends MY_Controller {
	   	function __construct(){
	    	parent::__construct();
	   	}

		public function index()
		{
			$this->load->view('template/home-template', $data);
		}

	  	public function homeTemplate($data=null){
	    	$this->load->view('template/home-template', $data);
	  	}

		public function adminTemplate($data=null){
			$this->load->view('template/admin-template', $data);
		}

		public function operatorTemplate($data=null){
			$this->load->view('template/operator-template', $data);
		}

		public function commonTemplate($data=null){
			$this->load->view('template/common-template', $data);
		}

		public function blogTemplate($data=null){
			$this->load->view('template/blog-template', $data);
		}
		public function managerTemplate($data=null){
			$this->load->view('template/manager-template', $data);
		}
}
