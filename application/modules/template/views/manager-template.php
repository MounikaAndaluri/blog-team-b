<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $this->config->item('site_name'); ?> | Manager Panel</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">



    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/bootstrap/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/dist/css/skins/_all-skins.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/iCheck/flat/blue.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/morris/morris.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/datepicker/datepicker3.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/daterangepicker/daterangepicker-bs3.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
	<!-- DataTables -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/datatables/dataTables.bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.1.2/css/buttons.dataTables.min.css" >


    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/select2/select2.min.css">

    <link href="<?php echo base_url(); ?>assets/admin/style.css" rel="stylesheet">

    <style type="text/css">
    #map-canvas {
  width: 500px;
  height: 400px;
    }
    #siteurl{

      display: none;
    }

    </style>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>




    	<body class="skin-blue sidebar-mini wysihtml5-supported">
        <span id="siteurl"><?php echo base_url(); ?></span>
    			 <div class="wrapper">

             <header class="main-header">
         <!-- Logo -->
         <a href="<?php echo base_url()."managers"; ?>" class="logo">
           <!-- mini logo for sidebar mini 50x50 pixels -->
           <span class="logo-mini">Manager</span>
           <!-- logo for regular state and mobile devices -->
           <span class="logo-lg"><b>Manager Control</b></span>
         </a>
         <!-- Header Navbar: style can be found in header.less -->
         <nav class="navbar navbar-static-top" role="navigation">
           <!-- Sidebar toggle button-->
           <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
             <span class="sr-only">Toggle navigation</span>
             <span class="icon-bar"></span>
             <span class="icon-bar"></span>
             <span class="icon-bar"></span>
           </a>
           <div class="navbar-custom-menu">
             <ul class="nav navbar-nav">
               <!-- Messages: style can be found in dropdown.less-->
               <li class="dropdown messages-menu">
                 <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                   <i class="fa fa-envelope-o"></i>
                   <span class="label label-success">0</span>
                 </a>

               </li>
               <!-- Notifications: style can be found in dropdown.less -->
               <li class="dropdown notifications-menu">
                 <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                   <i class="fa fa-bell-o"></i>
                   <span class="label label-warning">0</span>
                 </a>

               </li>
               <!-- Tasks: style can be found in dropdown.less -->
               <li class="dropdown tasks-menu">
                 <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                   <i class="fa fa-flag-o"></i>
                   <span class="label label-danger">0</span>
                 </a>

               </li>
               <!-- User Account: style can be found in dropdown.less -->
               <li class="dropdown user user-menu">
                  
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                   <img src="<?php echo base_url(); ?>assets/admin/dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                   <span class="hidden-xs"></span>
                  </a>
                  <ul class="dropdown-menu">
                   <!-- User image -->
                   <li class="user-header">
                     <img src="<?php echo base_url(); ?>assets/admin/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                     <p>
                      <?php echo $this->session->userdata['username']; ?>
                       <small>Manager</small>
                     </p>
                   </li>
                   <!-- Menu Footer-->
                   <li class="user-footer">
                     <div class="pull-left">
                       <a href="<?php echo base_url()."managers"; ?>" class="btn btn-default btn-flat">Profile</a>
                     </div>
                     <div class="pull-right">
                       <a href="<?php echo base_url()."members/logout"; ?>" class="btn btn-default btn-flat">Sign out</a>
                     </div>
                   </li>

                 </ul>
               </li>
             </ul>
           </div>
         </nav>
       </header>

       <!-- =============================================== -->

       <!-- Left side column. contains the sidebar -->
       <aside class="main-sidebar">
         <!-- sidebar: style can be found in sidebar.less -->
         <section class="sidebar">
           <!-- Sidebar user panel -->
           <div class="user-panel">
             <div class="pull-left image">
               <img src="<?php echo base_url(); ?>assets/admin/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
             </div>
             <div class="pull-left info">
               <p><?php echo $this->session->userdata['username']; ?></p>
               <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
             </div>
           </div>

           <!-- sidebar menu: : style can be found in sidebar.less -->
           <ul class="sidebar-menu">
             <li class="header">MAIN NAVIGATION</li>
             <li>
               <a href="<?php echo base_url()."managers"; ?>">
                 <i class="fa fa-dashboard"></i> <span>Dashboard</span> </i>
               </a>
             </li>

         

<li class="treeview">
                <a href="javascript:void(0);">
                  <i class="fa fa-credit-card-alt" aria-hidden="true"></i>
                 <span>Blog</span>
                 <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                 <li class="treeview">
                     <a href="javascript:void(0);">
                       <i class="fa fa-window-restore" aria-hidden="true"></i>
                       <span>Posts</span>
                       <i class="fa fa-angle-left pull-right"></i>
                     </a>
                     <ul class="treeview-menu">
                       <li><a href="<?php echo base_url()."managers/posts/add_post"; ?>"><i class="fa fa-file-powerpoint-o" aria-hidden="true"></i>Add Post</a></li>
                      <li><a href="<?php echo base_url()."managers/posts/"; ?>"><i class="fa fa-window-restore" aria-hidden="true"></i>All Posts</a></li>
                      <li><a href="<?php echo base_url()."managers/posts/deactivatePosts"; ?>"><i class="fa fa-trash" aria-hidden="true"></i>Deactivated Posts</a></li>
                      <li><a href="<?php echo base_url()."managers/posts/trashPosts"; ?>"><i class="fa fa-trash" aria-hidden="true"></i>Trash Posts</a></li>
                       
                     </ul>
                  </li>
                   <li class="treeview">
                     <a href="javascript:void();">
                      <i class="fa fa-bars" aria-hidden="true"></i>
                       <span>Categories</span>
                      
                     </a>
                     <ul class="treeview-menu">
                       <li><a href="<?php echo base_url()."managers/categories/add_category"; ?>"><i class="fa fa-file-powerpoint-o" aria-hidden="true"></i>Add category</a></li>
                      <li><a href="<?php echo base_url()."managers/categories/"; ?>"><i class="fa fa-window-restore" aria-hidden="true"></i>All Categories</a></li>
                       <li><a href="<?php echo base_url()."managers/categories/trashCategories"; ?>"><i class="fa fa-trash" aria-hidden="true"></i>Trash Categories</a></li>
                       
                     </ul>
                    
                  </li>
                 
                </ul>
            </li>

            <li>
               <a href="<?php echo base_url(); ?>" target="__blank">
                   <i class="fa fa-credit-card" aria-hidden="true"></i>
                 <span>Launch Website</span>
               </a>
            </li>
            
            <li>
                <a href="<?php echo base_url('blog');?>" target="__blank">
                   <i class="fa fa-credit-card-alt" aria-hidden="true"></i>
                   <span>Launch Blog</span>
                </a>
            </li>
             

           </ul>
         </section>
         <!-- /.sidebar -->
       </aside>

       <!-- =============================================== -->

       <!-- Content Wrapper. Contains page content -->
       <div class="content-wrapper">
              <?php echo $this->load->view($view); ?>
       </div><!-- /.content-wrapper -->

       <footer class="main-footer">
         <div class="pull-right hidden-xs">
           <b>Version</b> 1.0
         </div>
         <strong>Copyright &copy; 2017 <a href="<?php echo base_url(); ?>"><?php echo $this->config->item('site_name'); ?></a>.</strong> All rights reserved.
       </footer>






    			     </div>









        
        <script src="<?php echo base_url(); ?>assets/admin/plugins/jQuery/jQuery-2.1.4.min.js"></script>

    	<!-- jQuery UI 1.11.4 -->
    	<script src="<?php echo base_url(); ?>assets/admin/plugins/jQueryUI/jquery-ui.min.js"></script>



    	<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
	    <script>
	      $.widget.bridge('uibutton', $.ui.button);
	    </script>

        <script src="<?php echo base_url(); ?>assets/admin/bootstrap/js/bootstrap.min.js"></script>


	    <!-- Morris.js charts -->
	    <script src="<?php echo base_url(); ?>assets/admin/bootstrap/js/raphael-min.js"></script>
	    <script src="<?php echo base_url(); ?>assets/admin/plugins/morris/morris.min.js"></script>


	    <!-- Sparkline -->
	    <script src="<?php echo base_url(); ?>assets/admin/plugins/sparkline/jquery.sparkline.min.js"></script>
	    <!-- jvectormap -->
	    <script src="<?php echo base_url(); ?>assets/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	    <script src="<?php echo base_url(); ?>assets/admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	    <!-- jQuery Knob Chart -->
	    <script src="<?php echo base_url(); ?>assets/admin/plugins/knob/jquery.knob.js"></script>
	    <!-- daterangepicker -->
	    <script src="<?php echo base_url(); ?>assets/admin/bootstrap/js/moment.min.js"></script>
	    <script src="<?php echo base_url(); ?>assets/admin/plugins/daterangepicker/daterangepicker.js"></script>
	    <!-- datepicker -->
	    <script src="<?php echo base_url(); ?>assets/admin/plugins/datepicker/bootstrap-datepicker.js"></script>
	    <!-- Bootstrap WYSIHTML5 -->
	    <script src="<?php echo base_url(); ?>assets/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
	    <!-- Slimscroll -->
	    <script src="<?php echo base_url(); ?>assets/admin/plugins/slimScroll/jquery.slimscroll.min.js"></script>
	    <!-- FastClick -->
	    <script src="<?php echo base_url(); ?>assets/admin/plugins/fastclick/fastclick.min.js"></script>
	    <!-- AdminLTE App -->
	    <script src="<?php echo base_url(); ?>assets/admin/dist/js/app.min.js"></script>
	   <!-- DataTables -->
    	<script src="<?php echo base_url(); ?>assets/admin/plugins/datatables/jquery.dataTables.min.js"></script>
    	<script src="<?php echo base_url(); ?>assets/admin/plugins/datatables/dataTables.bootstrap.min.js"></script>
      <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.1.2/js/dataTables.buttons.min.js"></script>
	    <script src="//cdn.datatables.net/buttons/1.1.2/js/buttons.print.min.js"></script>

      <script src="<?php echo base_url(); ?>assets/admin/plugins/select2/select2.full.min.js"></script>

	   <!-- Jquery form validation http://www.formvalidator.net/ -->
	   <script src="<?php echo base_url(); ?>assets/admin/plugins/validation/form-validator/jquery.form-validator.min.js"></script>

<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCnM1ufTq_kUY0mw57jq8_LWChnNNW9MVc"></script>
<!-- 
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDiSi-5Td10fBXiCUh_S9gz5pa-dE9I794&libraries=places"></script> -->
	    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
	    <script src="<?php echo base_url(); ?>assets/admin/dist/js/pages/dashboard.js"></script>

	    <!-- AdminLTE for demo purposes -->
	    <script src="<?php echo base_url(); ?>assets/admin/dist/js/demo.js"></script>

      <script src="https://use.fontawesome.com/1fdd5d6436.js"></script>
		

  <script src='//cdn.tinymce.com/4/tinymce.min.js'></script>
      
      <script type="text/javascript">
          tinymce.init({
              selector: '.html5editor',
              height: 500,
              theme: 'modern',
              plugins: [
                'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
              ],
              toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
              toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
              image_advtab: true,
              templates: [
                { title: 'Test template 1', content: 'Test 1' },
                { title: 'Test template 2', content: 'Test 2' }
              ],
              content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css'
              ]
          });
      </script>

      <script type="text/javascript">
            $.validate({
               modules : 'file',

              });

        $(document).ready(function() {

            var siteurl = $('#siteurl').text();
           
            console.log(siteurl+"managers/categories/getCategoriesSugg");
                 /*  AUTO suggestion */
                    $( "#categories" ).autocomplete({
                      source: siteurl+"managers/categories/getCategoriesSugg",
                      
                      minLength: 1,

                      focus: function( event, ui ) {
                        $( "#categories" ).val( ui.item.category_name );
                        return false;
                      },
                      select: function( event, ui ) {
                        $( "#categoryid" ).val( ui.item.category_id );
                       // console.log(ui.item.user_id+","+ui.item.user_name);
                        return false;
                      }
                    })
                    .autocomplete( "instance" )._renderItem = function( ul, item ) {
                      //console.log(item.user_name);
                      $(ul).addClass('list-group');
                      return $( "<li class="+"list-group-item"+">" )
                        .append( "<div>" + item.category_name + "</div>" )
                        .appendTo( ul );
                    };

      });
      </script>
    </body>
</html>
