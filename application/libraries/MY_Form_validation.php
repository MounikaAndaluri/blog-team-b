<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class My_Form_validation extends CI_Form_validation {

        public function __construct()
        {
            parent::__construct();
        }
        
        public function resetpostdata()
        {
            $obj =& _get_validation_object();
            $_POST = array();
            foreach($obj->_field_data as $key)
            {
                $this->_field_data[$key['field']]['postdata'] = NULL;
            }
            return true;
        }

        function valid_year($date, $format){ // 'rules' => 'required|trim|valid_date[Y-m-d]',
            $d = DateTime::createFromFormat($format, $date);
            if($d && $d->format($format) == $date) {
                return true;
            } else {
                $this->set_message('valid_year', 'Date format must be YYYY eg: 2014');
                return false;
            }
        }

        function valid_date($date, $format){  // 'rules' => 'required|trim|valid_date[Y-m-d]',
            $d = DateTime::createFromFormat($format, $date);
            if($d && $d->format($format) == $date) {
                return true;
            } else {
                $this->set_message('valid_date', 'Date format must be YYYY-MM-DD eg: 2014-12-12');
                return false;
            }
        }

        function valid_date_time($date, $format){ // 'rules' => 'required|trim|valid_date_time[Y-m-d h:i:s]',
            $d = DateTime::createFromFormat($format, $date);
            if($d && $d->format($format) == $date) {
                return true;
            } else {
                $this->set_message('valid_date_time', 'Date format must be YYYY-MM-DD HH:MM:SS eg: 2014-12-12 11:11:11');
                return false;
            }
        }

        /**
        *  email rules
        *
        */
        function domain_email($str,$params)
        {
            $parts = explode("@", $str);
            $domain = $parts[1];
            $allowed_array = explode(',',$params);
            return in_array($domain, $allowed_array);
        }
        /**
         * Checks if the given domain has e-mailadresses by checking MX an A records
         * on the fly.
         *
         * It takes a little longer but will return false for input with valid
         * syntax but a non-existing domain.
         *
         * @param String $str
         * @return Boolean
        */
        function dns_email($str){

            // First, check syntax using CI valid_email function
            if (parent::valid_email($str)) { // email syntax is OK

                // This will split the email into its front
                // and back (the domain) portions
                list($name, $domain) = split('@',$str);

                // Check if domain has MX or A records (btw, this does NOT tell us if the
                // individual email address existst!)
                //return (!checkdnsrr($domain,'MX')) ? FALSE : TRUE;
                return (!(checkdnsrr($domain,"MX") || checkdnsrr($domain, "A"))) ? FALSE : TRUE;

            }
            else { // syntax is not OK
                return false;
            }
        }
        
         /**
         * Single Space
         *
         * - converts multiple spaces to one space
         * 
         * eg. "a      b" -> "a b"
         *
         * @access    public
         * @param    string
         * @return    void
         */
        function single_space($str)
        {
            $_POST[$this->_current_field] = preg_replace('/  +/', ' ', $str);
        }

        /**
         * Alpha-numeric with underscores, dashes and spaces
         *
         * @access    public
         * @param    string
         * @return    bool
         */    
        function alpha_dash_space($str)
        {
            return ( ! preg_match("/^([-a-z0-9_- ])+$/i", $str)) ? FALSE : TRUE;
        }

        /**
         * Edit_unique is used to check the unique value while editing 
         * @access public
         * @param $value
         * @param $params
         * 
         */
        //'rules' => 'required|edit_unique[categories.category_name.category_id.'.$cid.']',
        //$this->form_validation->set_rules('username', 'Username', 'required|edit_unique[users.username.primary_key.4]');
        function edit_unique($value, $params)
        {
        	$this->set_message('edit_unique', "This %s is already in use!");
        	list($table, $field, $primary_key, $current_id) = explode(".", $params);
        	$result = $this->CI->db->where($field, $value)->get($table)->row_array();
        	//echo $result->category_id."-".$current_id;
        	//exit();
        	return ($result && $result[$primary_key] != $current_id) ? FALSE : TRUE;
        }

    }
    ?>